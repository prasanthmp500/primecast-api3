package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * A Campaign.
 */
@Table(name = "campaign")
public class Campaign implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private UUID account;

    @NotNull
    private UUID createdBy;

    @NotNull
    private Instant startDate;

    @NotNull
    private Instant endDate;

    @NotNull
    private UUID type;

    @NotNull
    private Float budget;

    @NotNull
    private UUID status;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getAccount() {
        return account;
    }

    public Campaign account(UUID account) {
        this.account = account;
        return this;
    }

    public void setAccount(UUID account) {
        this.account = account;
    }

    public UUID getCreatedBy() {
        return createdBy;
    }

    public Campaign createdBy(UUID createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(UUID createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public Campaign startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public Campaign endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public UUID getType() {
        return type;
    }

    public Campaign type(UUID type) {
        this.type = type;
        return this;
    }

    public void setType(UUID type) {
        this.type = type;
    }

    public Float getBudget() {
        return budget;
    }

    public Campaign budget(Float budget) {
        this.budget = budget;
        return this;
    }

    public void setBudget(Float budget) {
        this.budget = budget;
    }

    public UUID getStatus() {
        return status;
    }

    public Campaign status(UUID status) {
        this.status = status;
        return this;
    }

    public void setStatus(UUID status) {
        this.status = status;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Campaign campaign = (Campaign) o;
        if (campaign.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaign.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Campaign{" +
            "id=" + getId() +
            ", account='" + getAccount() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", startDate='" + getStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", type='" + getType() + "'" +
            ", budget=" + getBudget() +
            ", status='" + getStatus() + "'" +
            "}";
    }
}
