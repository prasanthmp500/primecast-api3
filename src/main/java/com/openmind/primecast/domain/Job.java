package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;
import java.util.UUID;

/**
 * A Job.
 */
@Table(name = "job")
public class Job implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private UUID campaign;

    @NotNull
    private UUID createdBy;

    @NotNull
    private UUID status;

    private UUID contactlist;

    private UUID profile;

    private UUID personna;

    private UUID recipientGatheringList;

    private UUID location;

    @NotNull
    private UUID inventory;

    @NotNull
    private Instant startTimestamp;

    @NotNull
    private Instant endTimestamp;

    private Instant deferredStartDate;

    @NotNull
    private Instant endDate;

    private Boolean relativeValidityPeriod;

    private Instant validityPeriod;

    @NotNull
    private Integer impressionLimit;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getCampaign() {
        return campaign;
    }

    public Job campaign(UUID campaign) {
        this.campaign = campaign;
        return this;
    }

    public void setCampaign(UUID campaign) {
        this.campaign = campaign;
    }

    public UUID getCreatedBy() {
        return createdBy;
    }

    public Job createdBy(UUID createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public void setCreatedBy(UUID createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getStatus() {
        return status;
    }

    public Job status(UUID status) {
        this.status = status;
        return this;
    }

    public void setStatus(UUID status) {
        this.status = status;
    }

    public UUID getContactlist() {
        return contactlist;
    }

    public Job contactlist(UUID contactlist) {
        this.contactlist = contactlist;
        return this;
    }

    public void setContactlist(UUID contactlist) {
        this.contactlist = contactlist;
    }

    public UUID getProfile() {
        return profile;
    }

    public Job profile(UUID profile) {
        this.profile = profile;
        return this;
    }

    public void setProfile(UUID profile) {
        this.profile = profile;
    }

    public UUID getPersonna() {
        return personna;
    }

    public Job personna(UUID personna) {
        this.personna = personna;
        return this;
    }

    public void setPersonna(UUID personna) {
        this.personna = personna;
    }

    public UUID getRecipientGatheringList() {
        return recipientGatheringList;
    }

    public Job recipientGatheringList(UUID recipientGatheringList) {
        this.recipientGatheringList = recipientGatheringList;
        return this;
    }

    public void setRecipientGatheringList(UUID recipientGatheringList) {
        this.recipientGatheringList = recipientGatheringList;
    }

    public UUID getLocation() {
        return location;
    }

    public Job location(UUID location) {
        this.location = location;
        return this;
    }

    public void setLocation(UUID location) {
        this.location = location;
    }

    public UUID getInventory() {
        return inventory;
    }

    public Job inventory(UUID inventory) {
        this.inventory = inventory;
        return this;
    }

    public void setInventory(UUID inventory) {
        this.inventory = inventory;
    }

    public Instant getStartTimestamp() {
        return startTimestamp;
    }

    public Job startTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
        return this;
    }

    public void setStartTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Instant getEndTimestamp() {
        return endTimestamp;
    }

    public Job endTimestamp(Instant endTimestamp) {
        this.endTimestamp = endTimestamp;
        return this;
    }

    public void setEndTimestamp(Instant endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public Instant getDeferredStartDate() {
        return deferredStartDate;
    }

    public Job deferredStartDate(Instant deferredStartDate) {
        this.deferredStartDate = deferredStartDate;
        return this;
    }

    public void setDeferredStartDate(Instant deferredStartDate) {
        this.deferredStartDate = deferredStartDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public Job endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Boolean isRelativeValidityPeriod() {
        return relativeValidityPeriod;
    }

    public Job relativeValidityPeriod(Boolean relativeValidityPeriod) {
        this.relativeValidityPeriod = relativeValidityPeriod;
        return this;
    }

    public void setRelativeValidityPeriod(Boolean relativeValidityPeriod) {
        this.relativeValidityPeriod = relativeValidityPeriod;
    }

    public Instant getValidityPeriod() {
        return validityPeriod;
    }

    public Job validityPeriod(Instant validityPeriod) {
        this.validityPeriod = validityPeriod;
        return this;
    }

    public void setValidityPeriod(Instant validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public Integer getImpressionLimit() {
        return impressionLimit;
    }

    public Job impressionLimit(Integer impressionLimit) {
        this.impressionLimit = impressionLimit;
        return this;
    }

    public void setImpressionLimit(Integer impressionLimit) {
        this.impressionLimit = impressionLimit;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Job job = (Job) o;
        if (job.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), job.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Job{" +
            "id=" + getId() +
            ", campaign='" + getCampaign() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", status='" + getStatus() + "'" +
            ", contactlist='" + getContactlist() + "'" +
            ", profile='" + getProfile() + "'" +
            ", personna='" + getPersonna() + "'" +
            ", recipientGatheringList='" + getRecipientGatheringList() + "'" +
            ", location='" + getLocation() + "'" +
            ", inventory='" + getInventory() + "'" +
            ", startTimestamp='" + getStartTimestamp() + "'" +
            ", endTimestamp='" + getEndTimestamp() + "'" +
            ", deferredStartDate='" + getDeferredStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", relativeValidityPeriod='" + isRelativeValidityPeriod() + "'" +
            ", validityPeriod='" + getValidityPeriod() + "'" +
            ", impressionLimit=" + getImpressionLimit() +
            "}";
    }
}
