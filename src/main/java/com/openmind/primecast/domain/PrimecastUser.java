package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A PrimecastUser.
 */
@Table(name = "primecastUser")
public class PrimecastUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private UUID user;

    @NotNull
    private UUID account;

    private UUID language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUser() {
        return user;
    }

    public PrimecastUser user(UUID user) {
        this.user = user;
        return this;
    }

    public void setUser(UUID user) {
        this.user = user;
    }

    public UUID getAccount() {
        return account;
    }

    public PrimecastUser account(UUID account) {
        this.account = account;
        return this;
    }

    public void setAccount(UUID account) {
        this.account = account;
    }

    public UUID getLanguage() {
        return language;
    }

    public PrimecastUser language(UUID language) {
        this.language = language;
        return this;
    }

    public void setLanguage(UUID language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PrimecastUser primecastUser = (PrimecastUser) o;
        if (primecastUser.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), primecastUser.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PrimecastUser{" +
            "id=" + getId() +
            ", user='" + getUser() + "'" +
            ", account='" + getAccount() + "'" +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
