package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A PrimecastAccount.
 */
@Table(name = "primecastAccount")
public class PrimecastAccount implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private String name;

    private String address1;

    private String address2;

    private String address3;

    private String address4;

    private String country;

    @NotNull
    private String status;

    @NotNull
    private Boolean internal;

    private UUID currency;

    private UUID defaultLanguage;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public PrimecastAccount name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress1() {
        return address1;
    }

    public PrimecastAccount address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public PrimecastAccount address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getAddress3() {
        return address3;
    }

    public PrimecastAccount address3(String address3) {
        this.address3 = address3;
        return this;
    }

    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    public String getAddress4() {
        return address4;
    }

    public PrimecastAccount address4(String address4) {
        this.address4 = address4;
        return this;
    }

    public void setAddress4(String address4) {
        this.address4 = address4;
    }

    public String getCountry() {
        return country;
    }

    public PrimecastAccount country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getStatus() {
        return status;
    }

    public PrimecastAccount status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean isInternal() {
        return internal;
    }

    public PrimecastAccount internal(Boolean internal) {
        this.internal = internal;
        return this;
    }

    public void setInternal(Boolean internal) {
        this.internal = internal;
    }

    public UUID getCurrency() {
        return currency;
    }

    public PrimecastAccount currency(UUID currency) {
        this.currency = currency;
        return this;
    }

    public void setCurrency(UUID currency) {
        this.currency = currency;
    }

    public UUID getDefaultLanguage() {
        return defaultLanguage;
    }

    public PrimecastAccount defaultLanguage(UUID defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
        return this;
    }

    public void setDefaultLanguage(UUID defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        PrimecastAccount primecastAccount = (PrimecastAccount) o;
        if (primecastAccount.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), primecastAccount.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PrimecastAccount{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address1='" + getAddress1() + "'" +
            ", address2='" + getAddress2() + "'" +
            ", address3='" + getAddress3() + "'" +
            ", address4='" + getAddress4() + "'" +
            ", country='" + getCountry() + "'" +
            ", status='" + getStatus() + "'" +
            ", internal='" + isInternal() + "'" +
            ", currency='" + getCurrency() + "'" +
            ", defaultLanguage='" + getDefaultLanguage() + "'" +
            "}";
    }
}
