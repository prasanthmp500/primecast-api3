package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A Template.
 */
@Table(name = "template")
public class Template implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private String text;

    @NotNull
    private Boolean defaultTemplate;

    private String clickThrough;

    @NotNull
    private UUID job;

    private UUID language;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public Template text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isDefaultTemplate() {
        return defaultTemplate;
    }

    public Template defaultTemplate(Boolean defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
        return this;
    }

    public void setDefaultTemplate(Boolean defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }

    public String getClickThrough() {
        return clickThrough;
    }

    public Template clickThrough(String clickThrough) {
        this.clickThrough = clickThrough;
        return this;
    }

    public void setClickThrough(String clickThrough) {
        this.clickThrough = clickThrough;
    }

    public UUID getJob() {
        return job;
    }

    public Template job(UUID job) {
        this.job = job;
        return this;
    }

    public void setJob(UUID job) {
        this.job = job;
    }

    public UUID getLanguage() {
        return language;
    }

    public Template language(UUID language) {
        this.language = language;
        return this;
    }

    public void setLanguage(UUID language) {
        this.language = language;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Template template = (Template) o;
        if (template.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), template.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Template{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", defaultTemplate='" + isDefaultTemplate() + "'" +
            ", clickThrough='" + getClickThrough() + "'" +
            ", job='" + getJob() + "'" +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
