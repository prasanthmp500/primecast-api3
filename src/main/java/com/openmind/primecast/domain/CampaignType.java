package com.openmind.primecast.domain;

import com.datastax.driver.mapping.annotations.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A CampaignType.
 */
@Table(name = "campaignType")
public class CampaignType implements Serializable {

    private static final long serialVersionUID = 1L;

    @PartitionKey
    private UUID id;

    @NotNull
    private String name;

    @NotNull
    private Boolean marketing;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public CampaignType name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isMarketing() {
        return marketing;
    }

    public CampaignType marketing(Boolean marketing) {
        this.marketing = marketing;
        return this;
    }

    public void setMarketing(Boolean marketing) {
        this.marketing = marketing;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CampaignType campaignType = (CampaignType) o;
        if (campaignType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignType{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", marketing='" + isMarketing() + "'" +
            "}";
    }
}
