package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.CurrencyDTO;
import java.util.List;

/**
 * Service Interface for managing Currency.
 */
public interface CurrencyService {

    /**
     * Save a currency.
     *
     * @param currencyDTO the entity to save
     * @return the persisted entity
     */
    CurrencyDTO save(CurrencyDTO currencyDTO);

    /**
     * Get all the currencies.
     *
     * @return the list of entities
     */
    List<CurrencyDTO> findAll();

    /**
     * Get the "id" currency.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CurrencyDTO findOne(String id);

    /**
     * Delete the "id" currency.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
