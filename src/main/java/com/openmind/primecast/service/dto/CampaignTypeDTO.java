package com.openmind.primecast.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the CampaignType entity.
 */
public class CampaignTypeDTO implements Serializable {

    private UUID id;

    @NotNull
    private String name;

    @NotNull
    private Boolean marketing;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean isMarketing() {
        return marketing;
    }

    public void setMarketing(Boolean marketing) {
        this.marketing = marketing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CampaignTypeDTO campaignTypeDTO = (CampaignTypeDTO) o;
        if(campaignTypeDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), campaignTypeDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "CampaignTypeDTO{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", marketing='" + isMarketing() + "'" +
            "}";
    }
}
