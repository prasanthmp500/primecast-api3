package com.openmind.primecast.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the PrimecastUser entity.
 */
public class PrimecastUserDTO implements Serializable {

    private UUID id;

    @NotNull
    private UUID user;

    @NotNull
    private UUID account;

    private UUID language;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getUser() {
        return user;
    }

    public void setUser(UUID user) {
        this.user = user;
    }

    public UUID getAccount() {
        return account;
    }

    public void setAccount(UUID account) {
        this.account = account;
    }

    public UUID getLanguage() {
        return language;
    }

    public void setLanguage(UUID language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PrimecastUserDTO primecastUserDTO = (PrimecastUserDTO) o;
        if(primecastUserDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), primecastUserDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "PrimecastUserDTO{" +
            "id=" + getId() +
            ", user='" + getUser() + "'" +
            ", account='" + getAccount() + "'" +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
