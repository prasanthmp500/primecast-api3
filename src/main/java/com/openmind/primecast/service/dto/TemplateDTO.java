package com.openmind.primecast.service.dto;


import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Template entity.
 */
public class TemplateDTO implements Serializable {

    private UUID id;

    @NotNull
    private String text;

    @NotNull
    private Boolean defaultTemplate;

    private String clickThrough;

    @NotNull
    private UUID job;

    private UUID language;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean isDefaultTemplate() {
        return defaultTemplate;
    }

    public void setDefaultTemplate(Boolean defaultTemplate) {
        this.defaultTemplate = defaultTemplate;
    }

    public String getClickThrough() {
        return clickThrough;
    }

    public void setClickThrough(String clickThrough) {
        this.clickThrough = clickThrough;
    }

    public UUID getJob() {
        return job;
    }

    public void setJob(UUID job) {
        this.job = job;
    }

    public UUID getLanguage() {
        return language;
    }

    public void setLanguage(UUID language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TemplateDTO templateDTO = (TemplateDTO) o;
        if(templateDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), templateDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "TemplateDTO{" +
            "id=" + getId() +
            ", text='" + getText() + "'" +
            ", defaultTemplate='" + isDefaultTemplate() + "'" +
            ", clickThrough='" + getClickThrough() + "'" +
            ", job='" + getJob() + "'" +
            ", language='" + getLanguage() + "'" +
            "}";
    }
}
