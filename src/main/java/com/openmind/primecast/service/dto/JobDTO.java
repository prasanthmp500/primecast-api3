package com.openmind.primecast.service.dto;


import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

/**
 * A DTO for the Job entity.
 */
public class JobDTO implements Serializable {

    private UUID id;

    @NotNull
    private UUID campaign;

    @NotNull
    private UUID createdBy;

    @NotNull
    private UUID status;

    private UUID contactlist;

    private UUID profile;

    private UUID personna;

    private UUID recipientGatheringList;

    private UUID location;

    @NotNull
    private UUID inventory;

    @NotNull
    private Instant startTimestamp;

    @NotNull
    private Instant endTimestamp;

    private Instant deferredStartDate;

    @NotNull
    private Instant endDate;

    private Boolean relativeValidityPeriod;

    private Instant validityPeriod;

    @NotNull
    private Integer impressionLimit;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getCampaign() {
        return campaign;
    }

    public void setCampaign(UUID campaign) {
        this.campaign = campaign;
    }

    public UUID getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UUID createdBy) {
        this.createdBy = createdBy;
    }

    public UUID getStatus() {
        return status;
    }

    public void setStatus(UUID status) {
        this.status = status;
    }

    public UUID getContactlist() {
        return contactlist;
    }

    public void setContactlist(UUID contactlist) {
        this.contactlist = contactlist;
    }

    public UUID getProfile() {
        return profile;
    }

    public void setProfile(UUID profile) {
        this.profile = profile;
    }

    public UUID getPersonna() {
        return personna;
    }

    public void setPersonna(UUID personna) {
        this.personna = personna;
    }

    public UUID getRecipientGatheringList() {
        return recipientGatheringList;
    }

    public void setRecipientGatheringList(UUID recipientGatheringList) {
        this.recipientGatheringList = recipientGatheringList;
    }

    public UUID getLocation() {
        return location;
    }

    public void setLocation(UUID location) {
        this.location = location;
    }

    public UUID getInventory() {
        return inventory;
    }

    public void setInventory(UUID inventory) {
        this.inventory = inventory;
    }

    public Instant getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(Instant startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public Instant getEndTimestamp() {
        return endTimestamp;
    }

    public void setEndTimestamp(Instant endTimestamp) {
        this.endTimestamp = endTimestamp;
    }

    public Instant getDeferredStartDate() {
        return deferredStartDate;
    }

    public void setDeferredStartDate(Instant deferredStartDate) {
        this.deferredStartDate = deferredStartDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Boolean isRelativeValidityPeriod() {
        return relativeValidityPeriod;
    }

    public void setRelativeValidityPeriod(Boolean relativeValidityPeriod) {
        this.relativeValidityPeriod = relativeValidityPeriod;
    }

    public Instant getValidityPeriod() {
        return validityPeriod;
    }

    public void setValidityPeriod(Instant validityPeriod) {
        this.validityPeriod = validityPeriod;
    }

    public Integer getImpressionLimit() {
        return impressionLimit;
    }

    public void setImpressionLimit(Integer impressionLimit) {
        this.impressionLimit = impressionLimit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobDTO jobDTO = (JobDTO) o;
        if(jobDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), jobDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "JobDTO{" +
            "id=" + getId() +
            ", campaign='" + getCampaign() + "'" +
            ", createdBy='" + getCreatedBy() + "'" +
            ", status='" + getStatus() + "'" +
            ", contactlist='" + getContactlist() + "'" +
            ", profile='" + getProfile() + "'" +
            ", personna='" + getPersonna() + "'" +
            ", recipientGatheringList='" + getRecipientGatheringList() + "'" +
            ", location='" + getLocation() + "'" +
            ", inventory='" + getInventory() + "'" +
            ", startTimestamp='" + getStartTimestamp() + "'" +
            ", endTimestamp='" + getEndTimestamp() + "'" +
            ", deferredStartDate='" + getDeferredStartDate() + "'" +
            ", endDate='" + getEndDate() + "'" +
            ", relativeValidityPeriod='" + isRelativeValidityPeriod() + "'" +
            ", validityPeriod='" + getValidityPeriod() + "'" +
            ", impressionLimit=" + getImpressionLimit() +
            "}";
    }
}
