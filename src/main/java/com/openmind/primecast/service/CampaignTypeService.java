package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.CampaignTypeDTO;
import java.util.List;

/**
 * Service Interface for managing CampaignType.
 */
public interface CampaignTypeService {

    /**
     * Save a campaignType.
     *
     * @param campaignTypeDTO the entity to save
     * @return the persisted entity
     */
    CampaignTypeDTO save(CampaignTypeDTO campaignTypeDTO);

    /**
     * Get all the campaignTypes.
     *
     * @return the list of entities
     */
    List<CampaignTypeDTO> findAll();

    /**
     * Get the "id" campaignType.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CampaignTypeDTO findOne(String id);

    /**
     * Delete the "id" campaignType.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
