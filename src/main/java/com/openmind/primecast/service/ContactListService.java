package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.ContactListDTO;
import java.util.List;

/**
 * Service Interface for managing ContactList.
 */
public interface ContactListService {

    /**
     * Save a contactList.
     *
     * @param contactListDTO the entity to save
     * @return the persisted entity
     */
    ContactListDTO save(ContactListDTO contactListDTO);

    /**
     * Get all the contactLists.
     *
     * @return the list of entities
     */
    List<ContactListDTO> findAll();

    /**
     * Get the "id" contactList.
     *
     * @param id the id of the entity
     * @return the entity
     */
    ContactListDTO findOne(String id);

    /**
     * Delete the "id" contactList.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
