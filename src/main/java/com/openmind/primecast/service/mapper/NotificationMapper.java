package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.NotificationDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Notification and its DTO NotificationDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface NotificationMapper extends EntityMapper<NotificationDTO, Notification> {


}
