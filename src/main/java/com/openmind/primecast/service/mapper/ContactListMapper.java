package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.ContactListDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity ContactList and its DTO ContactListDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ContactListMapper extends EntityMapper<ContactListDTO, ContactList> {


}
