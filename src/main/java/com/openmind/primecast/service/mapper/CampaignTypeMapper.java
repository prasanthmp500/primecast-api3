package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.CampaignTypeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignType and its DTO CampaignTypeDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CampaignTypeMapper extends EntityMapper<CampaignTypeDTO, CampaignType> {


}
