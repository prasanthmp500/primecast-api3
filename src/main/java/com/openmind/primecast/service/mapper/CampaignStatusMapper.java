package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.CampaignStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity CampaignStatus and its DTO CampaignStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface CampaignStatusMapper extends EntityMapper<CampaignStatusDTO, CampaignStatus> {


}
