package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.PrimecastUserDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PrimecastUser and its DTO PrimecastUserDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PrimecastUserMapper extends EntityMapper<PrimecastUserDTO, PrimecastUser> {


}
