package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.FlightStatusDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity FlightStatus and its DTO FlightStatusDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FlightStatusMapper extends EntityMapper<FlightStatusDTO, FlightStatus> {


}
