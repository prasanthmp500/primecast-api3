package com.openmind.primecast.service.mapper;

import com.openmind.primecast.domain.*;
import com.openmind.primecast.service.dto.PrimecastAccountDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PrimecastAccount and its DTO PrimecastAccountDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface PrimecastAccountMapper extends EntityMapper<PrimecastAccountDTO, PrimecastAccount> {


}
