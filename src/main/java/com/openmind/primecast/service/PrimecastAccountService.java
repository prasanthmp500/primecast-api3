package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.PrimecastAccountDTO;
import java.util.List;

/**
 * Service Interface for managing PrimecastAccount.
 */
public interface PrimecastAccountService {

    /**
     * Save a primecastAccount.
     *
     * @param primecastAccountDTO the entity to save
     * @return the persisted entity
     */
    PrimecastAccountDTO save(PrimecastAccountDTO primecastAccountDTO);

    /**
     * Get all the primecastAccounts.
     *
     * @return the list of entities
     */
    List<PrimecastAccountDTO> findAll();

    /**
     * Get the "id" primecastAccount.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PrimecastAccountDTO findOne(String id);

    /**
     * Delete the "id" primecastAccount.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
