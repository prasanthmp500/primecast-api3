package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.PrimecastUserDTO;
import java.util.List;

/**
 * Service Interface for managing PrimecastUser.
 */
public interface PrimecastUserService {

    /**
     * Save a primecastUser.
     *
     * @param primecastUserDTO the entity to save
     * @return the persisted entity
     */
    PrimecastUserDTO save(PrimecastUserDTO primecastUserDTO);

    /**
     * Get all the primecastUsers.
     *
     * @return the list of entities
     */
    List<PrimecastUserDTO> findAll();

    /**
     * Get the "id" primecastUser.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PrimecastUserDTO findOne(String id);

    /**
     * Delete the "id" primecastUser.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
