package com.openmind.primecast.service.enumeration;

public enum AccountStatus {
    PENDING, ACTIVE, DISABLED
}
