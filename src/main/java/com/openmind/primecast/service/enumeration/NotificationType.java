package com.openmind.primecast.service.enumeration;

public enum NotificationType {
    APPROVED, DECLINED
}
