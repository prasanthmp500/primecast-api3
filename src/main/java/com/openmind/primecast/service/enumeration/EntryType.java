package com.openmind.primecast.service.enumeration;

public enum EntryType {
    EMAIL, PHONE_NUMBER, USERNAME
}
