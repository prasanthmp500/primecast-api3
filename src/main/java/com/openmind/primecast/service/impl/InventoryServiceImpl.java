package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.InventoryService;
import com.openmind.primecast.domain.Inventory;
import com.openmind.primecast.repository.InventoryRepository;
import com.openmind.primecast.service.dto.InventoryDTO;
import com.openmind.primecast.service.mapper.InventoryMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Inventory.
 */
@Service
public class InventoryServiceImpl implements InventoryService {

    private final Logger log = LoggerFactory.getLogger(InventoryServiceImpl.class);

    private final InventoryRepository inventoryRepository;

    private final InventoryMapper inventoryMapper;

    public InventoryServiceImpl(InventoryRepository inventoryRepository, InventoryMapper inventoryMapper) {
        this.inventoryRepository = inventoryRepository;
        this.inventoryMapper = inventoryMapper;
    }

    /**
     * Save a inventory.
     *
     * @param inventoryDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public InventoryDTO save(InventoryDTO inventoryDTO) {
        log.debug("Request to save Inventory : {}", inventoryDTO);
        Inventory inventory = inventoryMapper.toEntity(inventoryDTO);
        inventory = inventoryRepository.save(inventory);
        return inventoryMapper.toDto(inventory);
    }

    /**
     * Get all the inventories.
     *
     * @return the list of entities
     */
    @Override
    public List<InventoryDTO> findAll() {
        log.debug("Request to get all Inventories");
        return inventoryRepository.findAll().stream()
            .map(inventoryMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one inventory by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public InventoryDTO findOne(String id) {
        log.debug("Request to get Inventory : {}", id);
        Inventory inventory = inventoryRepository.findOne(UUID.fromString(id));
        return inventoryMapper.toDto(inventory);
    }

    /**
     * Delete the inventory by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Inventory : {}", id);
        inventoryRepository.delete(UUID.fromString(id));
    }
}
