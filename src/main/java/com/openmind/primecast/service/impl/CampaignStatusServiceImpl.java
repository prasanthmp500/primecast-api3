package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.CampaignStatusService;
import com.openmind.primecast.domain.CampaignStatus;
import com.openmind.primecast.repository.CampaignStatusRepository;
import com.openmind.primecast.service.dto.CampaignStatusDTO;
import com.openmind.primecast.service.mapper.CampaignStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CampaignStatus.
 */
@Service
public class CampaignStatusServiceImpl implements CampaignStatusService {

    private final Logger log = LoggerFactory.getLogger(CampaignStatusServiceImpl.class);

    private final CampaignStatusRepository campaignStatusRepository;

    private final CampaignStatusMapper campaignStatusMapper;

    public CampaignStatusServiceImpl(CampaignStatusRepository campaignStatusRepository, CampaignStatusMapper campaignStatusMapper) {
        this.campaignStatusRepository = campaignStatusRepository;
        this.campaignStatusMapper = campaignStatusMapper;
    }

    /**
     * Save a campaignStatus.
     *
     * @param campaignStatusDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignStatusDTO save(CampaignStatusDTO campaignStatusDTO) {
        log.debug("Request to save CampaignStatus : {}", campaignStatusDTO);
        CampaignStatus campaignStatus = campaignStatusMapper.toEntity(campaignStatusDTO);
        campaignStatus = campaignStatusRepository.save(campaignStatus);
        return campaignStatusMapper.toDto(campaignStatus);
    }

    /**
     * Get all the campaignStatuses.
     *
     * @return the list of entities
     */
    @Override
    public List<CampaignStatusDTO> findAll() {
        log.debug("Request to get all CampaignStatuses");
        return campaignStatusRepository.findAll().stream()
            .map(campaignStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one campaignStatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public CampaignStatusDTO findOne(String id) {
        log.debug("Request to get CampaignStatus : {}", id);
        CampaignStatus campaignStatus = campaignStatusRepository.findOne(UUID.fromString(id));
        return campaignStatusMapper.toDto(campaignStatus);
    }

    /**
     * Delete the campaignStatus by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete CampaignStatus : {}", id);
        campaignStatusRepository.delete(UUID.fromString(id));
    }
}
