package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.CampaignService;
import com.openmind.primecast.domain.Campaign;
import com.openmind.primecast.repository.CampaignRepository;
import com.openmind.primecast.service.dto.CampaignDTO;
import com.openmind.primecast.service.mapper.CampaignMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Campaign.
 */
@Service
public class CampaignServiceImpl implements CampaignService {

    private final Logger log = LoggerFactory.getLogger(CampaignServiceImpl.class);

    private final CampaignRepository campaignRepository;

    private final CampaignMapper campaignMapper;

    public CampaignServiceImpl(CampaignRepository campaignRepository, CampaignMapper campaignMapper) {
        this.campaignRepository = campaignRepository;
        this.campaignMapper = campaignMapper;
    }

    /**
     * Save a campaign.
     *
     * @param campaignDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignDTO save(CampaignDTO campaignDTO) {
        log.debug("Request to save Campaign : {}", campaignDTO);
        Campaign campaign = campaignMapper.toEntity(campaignDTO);
        campaign = campaignRepository.save(campaign);
        return campaignMapper.toDto(campaign);
    }

    /**
     * Get all the campaigns.
     *
     * @return the list of entities
     */
    @Override
    public List<CampaignDTO> findAll() {
        log.debug("Request to get all Campaigns");
        return campaignRepository.findAll().stream()
            .map(campaignMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one campaign by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public CampaignDTO findOne(String id) {
        log.debug("Request to get Campaign : {}", id);
        Campaign campaign = campaignRepository.findOne(UUID.fromString(id));
        return campaignMapper.toDto(campaign);
    }

    /**
     * Delete the campaign by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Campaign : {}", id);
        campaignRepository.delete(UUID.fromString(id));
    }
}
