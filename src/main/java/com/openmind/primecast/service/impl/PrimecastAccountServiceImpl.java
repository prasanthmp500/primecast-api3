package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.PrimecastAccountService;
import com.openmind.primecast.domain.PrimecastAccount;
import com.openmind.primecast.repository.PrimecastAccountRepository;
import com.openmind.primecast.service.dto.PrimecastAccountDTO;
import com.openmind.primecast.service.mapper.PrimecastAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PrimecastAccount.
 */
@Service
public class PrimecastAccountServiceImpl implements PrimecastAccountService {

    private final Logger log = LoggerFactory.getLogger(PrimecastAccountServiceImpl.class);

    private final PrimecastAccountRepository primecastAccountRepository;

    private final PrimecastAccountMapper primecastAccountMapper;

    public PrimecastAccountServiceImpl(PrimecastAccountRepository primecastAccountRepository, PrimecastAccountMapper primecastAccountMapper) {
        this.primecastAccountRepository = primecastAccountRepository;
        this.primecastAccountMapper = primecastAccountMapper;
    }

    /**
     * Save a primecastAccount.
     *
     * @param primecastAccountDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PrimecastAccountDTO save(PrimecastAccountDTO primecastAccountDTO) {
        log.debug("Request to save PrimecastAccount : {}", primecastAccountDTO);
        PrimecastAccount primecastAccount = primecastAccountMapper.toEntity(primecastAccountDTO);
        primecastAccount = primecastAccountRepository.save(primecastAccount);
        return primecastAccountMapper.toDto(primecastAccount);
    }

    /**
     * Get all the primecastAccounts.
     *
     * @return the list of entities
     */
    @Override
    public List<PrimecastAccountDTO> findAll() {
        log.debug("Request to get all PrimecastAccounts");
        return primecastAccountRepository.findAll().stream()
            .map(primecastAccountMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one primecastAccount by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public PrimecastAccountDTO findOne(String id) {
        log.debug("Request to get PrimecastAccount : {}", id);
        PrimecastAccount primecastAccount = primecastAccountRepository.findOne(UUID.fromString(id));
        return primecastAccountMapper.toDto(primecastAccount);
    }

    /**
     * Delete the primecastAccount by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete PrimecastAccount : {}", id);
        primecastAccountRepository.delete(UUID.fromString(id));
    }
}
