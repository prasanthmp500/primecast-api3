package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.PrimecastUserService;
import com.openmind.primecast.domain.PrimecastUser;
import com.openmind.primecast.repository.PrimecastUserRepository;
import com.openmind.primecast.service.dto.PrimecastUserDTO;
import com.openmind.primecast.service.mapper.PrimecastUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing PrimecastUser.
 */
@Service
public class PrimecastUserServiceImpl implements PrimecastUserService {

    private final Logger log = LoggerFactory.getLogger(PrimecastUserServiceImpl.class);

    private final PrimecastUserRepository primecastUserRepository;

    private final PrimecastUserMapper primecastUserMapper;

    public PrimecastUserServiceImpl(PrimecastUserRepository primecastUserRepository, PrimecastUserMapper primecastUserMapper) {
        this.primecastUserRepository = primecastUserRepository;
        this.primecastUserMapper = primecastUserMapper;
    }

    /**
     * Save a primecastUser.
     *
     * @param primecastUserDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public PrimecastUserDTO save(PrimecastUserDTO primecastUserDTO) {
        log.debug("Request to save PrimecastUser : {}", primecastUserDTO);
        PrimecastUser primecastUser = primecastUserMapper.toEntity(primecastUserDTO);
        primecastUser = primecastUserRepository.save(primecastUser);
        return primecastUserMapper.toDto(primecastUser);
    }

    /**
     * Get all the primecastUsers.
     *
     * @return the list of entities
     */
    @Override
    public List<PrimecastUserDTO> findAll() {
        log.debug("Request to get all PrimecastUsers");
        return primecastUserRepository.findAll().stream()
            .map(primecastUserMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one primecastUser by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public PrimecastUserDTO findOne(String id) {
        log.debug("Request to get PrimecastUser : {}", id);
        PrimecastUser primecastUser = primecastUserRepository.findOne(UUID.fromString(id));
        return primecastUserMapper.toDto(primecastUser);
    }

    /**
     * Delete the primecastUser by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete PrimecastUser : {}", id);
        primecastUserRepository.delete(UUID.fromString(id));
    }
}
