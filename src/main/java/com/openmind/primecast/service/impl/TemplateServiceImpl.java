package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.TemplateService;
import com.openmind.primecast.domain.Template;
import com.openmind.primecast.repository.TemplateRepository;
import com.openmind.primecast.service.dto.TemplateDTO;
import com.openmind.primecast.service.mapper.TemplateMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Template.
 */
@Service
public class TemplateServiceImpl implements TemplateService {

    private final Logger log = LoggerFactory.getLogger(TemplateServiceImpl.class);

    private final TemplateRepository templateRepository;

    private final TemplateMapper templateMapper;

    public TemplateServiceImpl(TemplateRepository templateRepository, TemplateMapper templateMapper) {
        this.templateRepository = templateRepository;
        this.templateMapper = templateMapper;
    }

    /**
     * Save a template.
     *
     * @param templateDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public TemplateDTO save(TemplateDTO templateDTO) {
        log.debug("Request to save Template : {}", templateDTO);
        Template template = templateMapper.toEntity(templateDTO);
        template = templateRepository.save(template);
        return templateMapper.toDto(template);
    }

    /**
     * Get all the templates.
     *
     * @return the list of entities
     */
    @Override
    public List<TemplateDTO> findAll() {
        log.debug("Request to get all Templates");
        return templateRepository.findAll().stream()
            .map(templateMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one template by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public TemplateDTO findOne(String id) {
        log.debug("Request to get Template : {}", id);
        Template template = templateRepository.findOne(UUID.fromString(id));
        return templateMapper.toDto(template);
    }

    /**
     * Delete the template by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete Template : {}", id);
        templateRepository.delete(UUID.fromString(id));
    }
}
