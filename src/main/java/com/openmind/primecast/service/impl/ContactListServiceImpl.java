package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.ContactListService;
import com.openmind.primecast.domain.ContactList;
import com.openmind.primecast.repository.ContactListRepository;
import com.openmind.primecast.service.dto.ContactListDTO;
import com.openmind.primecast.service.mapper.ContactListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing ContactList.
 */
@Service
public class ContactListServiceImpl implements ContactListService {

    private final Logger log = LoggerFactory.getLogger(ContactListServiceImpl.class);

    private final ContactListRepository contactListRepository;

    private final ContactListMapper contactListMapper;

    public ContactListServiceImpl(ContactListRepository contactListRepository, ContactListMapper contactListMapper) {
        this.contactListRepository = contactListRepository;
        this.contactListMapper = contactListMapper;
    }

    /**
     * Save a contactList.
     *
     * @param contactListDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public ContactListDTO save(ContactListDTO contactListDTO) {
        log.debug("Request to save ContactList : {}", contactListDTO);
        ContactList contactList = contactListMapper.toEntity(contactListDTO);
        contactList = contactListRepository.save(contactList);
        return contactListMapper.toDto(contactList);
    }

    /**
     * Get all the contactLists.
     *
     * @return the list of entities
     */
    @Override
    public List<ContactListDTO> findAll() {
        log.debug("Request to get all ContactLists");
        return contactListRepository.findAll().stream()
            .map(contactListMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one contactList by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public ContactListDTO findOne(String id) {
        log.debug("Request to get ContactList : {}", id);
        ContactList contactList = contactListRepository.findOne(UUID.fromString(id));
        return contactListMapper.toDto(contactList);
    }

    /**
     * Delete the contactList by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete ContactList : {}", id);
        contactListRepository.delete(UUID.fromString(id));
    }
}
