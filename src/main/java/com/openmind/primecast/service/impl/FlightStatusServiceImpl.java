package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.FlightStatusService;
import com.openmind.primecast.domain.FlightStatus;
import com.openmind.primecast.repository.FlightStatusRepository;
import com.openmind.primecast.service.dto.FlightStatusDTO;
import com.openmind.primecast.service.mapper.FlightStatusMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing FlightStatus.
 */
@Service
public class FlightStatusServiceImpl implements FlightStatusService {

    private final Logger log = LoggerFactory.getLogger(FlightStatusServiceImpl.class);

    private final FlightStatusRepository flightStatusRepository;

    private final FlightStatusMapper flightStatusMapper;

    public FlightStatusServiceImpl(FlightStatusRepository flightStatusRepository, FlightStatusMapper flightStatusMapper) {
        this.flightStatusRepository = flightStatusRepository;
        this.flightStatusMapper = flightStatusMapper;
    }

    /**
     * Save a flightStatus.
     *
     * @param flightStatusDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public FlightStatusDTO save(FlightStatusDTO flightStatusDTO) {
        log.debug("Request to save FlightStatus : {}", flightStatusDTO);
        FlightStatus flightStatus = flightStatusMapper.toEntity(flightStatusDTO);
        flightStatus = flightStatusRepository.save(flightStatus);
        return flightStatusMapper.toDto(flightStatus);
    }

    /**
     * Get all the flightStatuses.
     *
     * @return the list of entities
     */
    @Override
    public List<FlightStatusDTO> findAll() {
        log.debug("Request to get all FlightStatuses");
        return flightStatusRepository.findAll().stream()
            .map(flightStatusMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one flightStatus by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public FlightStatusDTO findOne(String id) {
        log.debug("Request to get FlightStatus : {}", id);
        FlightStatus flightStatus = flightStatusRepository.findOne(UUID.fromString(id));
        return flightStatusMapper.toDto(flightStatus);
    }

    /**
     * Delete the flightStatus by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete FlightStatus : {}", id);
        flightStatusRepository.delete(UUID.fromString(id));
    }
}
