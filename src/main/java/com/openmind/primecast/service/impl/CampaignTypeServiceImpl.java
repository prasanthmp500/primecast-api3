package com.openmind.primecast.service.impl;

import com.openmind.primecast.service.CampaignTypeService;
import com.openmind.primecast.domain.CampaignType;
import com.openmind.primecast.repository.CampaignTypeRepository;
import com.openmind.primecast.service.dto.CampaignTypeDTO;
import com.openmind.primecast.service.mapper.CampaignTypeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing CampaignType.
 */
@Service
public class CampaignTypeServiceImpl implements CampaignTypeService {

    private final Logger log = LoggerFactory.getLogger(CampaignTypeServiceImpl.class);

    private final CampaignTypeRepository campaignTypeRepository;

    private final CampaignTypeMapper campaignTypeMapper;

    public CampaignTypeServiceImpl(CampaignTypeRepository campaignTypeRepository, CampaignTypeMapper campaignTypeMapper) {
        this.campaignTypeRepository = campaignTypeRepository;
        this.campaignTypeMapper = campaignTypeMapper;
    }

    /**
     * Save a campaignType.
     *
     * @param campaignTypeDTO the entity to save
     * @return the persisted entity
     */
    @Override
    public CampaignTypeDTO save(CampaignTypeDTO campaignTypeDTO) {
        log.debug("Request to save CampaignType : {}", campaignTypeDTO);
        CampaignType campaignType = campaignTypeMapper.toEntity(campaignTypeDTO);
        campaignType = campaignTypeRepository.save(campaignType);
        return campaignTypeMapper.toDto(campaignType);
    }

    /**
     * Get all the campaignTypes.
     *
     * @return the list of entities
     */
    @Override
    public List<CampaignTypeDTO> findAll() {
        log.debug("Request to get all CampaignTypes");
        return campaignTypeRepository.findAll().stream()
            .map(campaignTypeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one campaignType by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    public CampaignTypeDTO findOne(String id) {
        log.debug("Request to get CampaignType : {}", id);
        CampaignType campaignType = campaignTypeRepository.findOne(UUID.fromString(id));
        return campaignTypeMapper.toDto(campaignType);
    }

    /**
     * Delete the campaignType by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(String id) {
        log.debug("Request to delete CampaignType : {}", id);
        campaignTypeRepository.delete(UUID.fromString(id));
    }
}
