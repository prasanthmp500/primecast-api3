package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.FlightStatusDTO;
import java.util.List;

/**
 * Service Interface for managing FlightStatus.
 */
public interface FlightStatusService {

    /**
     * Save a flightStatus.
     *
     * @param flightStatusDTO the entity to save
     * @return the persisted entity
     */
    FlightStatusDTO save(FlightStatusDTO flightStatusDTO);

    /**
     * Get all the flightStatuses.
     *
     * @return the list of entities
     */
    List<FlightStatusDTO> findAll();

    /**
     * Get the "id" flightStatus.
     *
     * @param id the id of the entity
     * @return the entity
     */
    FlightStatusDTO findOne(String id);

    /**
     * Delete the "id" flightStatus.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
