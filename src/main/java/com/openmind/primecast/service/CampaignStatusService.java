package com.openmind.primecast.service;

import com.openmind.primecast.service.dto.CampaignStatusDTO;
import java.util.List;

/**
 * Service Interface for managing CampaignStatus.
 */
public interface CampaignStatusService {

    /**
     * Save a campaignStatus.
     *
     * @param campaignStatusDTO the entity to save
     * @return the persisted entity
     */
    CampaignStatusDTO save(CampaignStatusDTO campaignStatusDTO);

    /**
     * Get all the campaignStatuses.
     *
     * @return the list of entities
     */
    List<CampaignStatusDTO> findAll();

    /**
     * Get the "id" campaignStatus.
     *
     * @param id the id of the entity
     * @return the entity
     */
    CampaignStatusDTO findOne(String id);

    /**
     * Delete the "id" campaignStatus.
     *
     * @param id the id of the entity
     */
    void delete(String id);
}
