package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Campaign;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Campaign entity.
 */
@Repository
public class CampaignRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Campaign> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public CampaignRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Campaign.class);
        this.findAllStmt = session.prepare("SELECT * FROM campaign");
        this.truncateStmt = session.prepare("TRUNCATE campaign");
    }

    public List<Campaign> findAll() {
        List<Campaign> campaignsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Campaign campaign = new Campaign();
                campaign.setId(row.getUUID("id"));
                campaign.setAccount(row.getUUID("account"));
                campaign.setCreatedBy(row.getUUID("createdBy"));
                campaign.setStartDate(row.get("startDate", Instant.class));
                campaign.setEndDate(row.get("endDate", Instant.class));
                campaign.setType(row.getUUID("type"));
                campaign.setBudget(row.getFloat("budget"));
                campaign.setStatus(row.getUUID("status"));
                return campaign;
            }
        ).forEach(campaignsList::add);
        return campaignsList;
    }

    public Campaign findOne(UUID id) {
        return mapper.get(id);
    }

    public Campaign save(Campaign campaign) {
        if (campaign.getId() == null) {
            campaign.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Campaign>> violations = validator.validate(campaign);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(campaign);
        return campaign;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
