package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Currency;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Currency entity.
 */
@Repository
public class CurrencyRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Currency> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public CurrencyRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Currency.class);
        this.findAllStmt = session.prepare("SELECT * FROM currency");
        this.truncateStmt = session.prepare("TRUNCATE currency");
    }

    public List<Currency> findAll() {
        List<Currency> currenciesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Currency currency = new Currency();
                currency.setId(row.getUUID("id"));
                currency.setName(row.getString("name"));
                currency.setCode(row.getString("code"));
                return currency;
            }
        ).forEach(currenciesList::add);
        return currenciesList;
    }

    public Currency findOne(UUID id) {
        return mapper.get(id);
    }

    public Currency save(Currency currency) {
        if (currency.getId() == null) {
            currency.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Currency>> violations = validator.validate(currency);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(currency);
        return currency;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
