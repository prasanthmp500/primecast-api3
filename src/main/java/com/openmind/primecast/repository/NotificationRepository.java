package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Notification;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Notification entity.
 */
@Repository
public class NotificationRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Notification> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public NotificationRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Notification.class);
        this.findAllStmt = session.prepare("SELECT * FROM notification");
        this.truncateStmt = session.prepare("TRUNCATE notification");
    }

    public List<Notification> findAll() {
        List<Notification> notificationsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Notification notification = new Notification();
                notification.setId(row.getUUID("id"));
                notification.setTitle(row.getString("title"));
                notification.setDescription(row.getString("description"));
                notification.setType(row.getString("type"));
                notification.setReferenceId(row.getUUID("referenceId"));
                notification.setRead(row.getBool("read"));
                notification.setUser(row.getUUID("user"));
                notification.setCreated(row.get("created", Instant.class));
                return notification;
            }
        ).forEach(notificationsList::add);
        return notificationsList;
    }

    public Notification findOne(UUID id) {
        return mapper.get(id);
    }

    public Notification save(Notification notification) {
        if (notification.getId() == null) {
            notification.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Notification>> violations = validator.validate(notification);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(notification);
        return notification;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
