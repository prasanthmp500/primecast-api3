package com.openmind.primecast.repository;

import com.openmind.primecast.domain.ContactList;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the ContactList entity.
 */
@Repository
public class ContactListRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<ContactList> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public ContactListRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(ContactList.class);
        this.findAllStmt = session.prepare("SELECT * FROM contactList");
        this.truncateStmt = session.prepare("TRUNCATE contactList");
    }

    public List<ContactList> findAll() {
        List<ContactList> contactListsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                ContactList contactList = new ContactList();
                contactList.setId(row.getUUID("id"));
                contactList.setType(row.getString("type"));
                contactList.setPath(row.getString("path"));
                return contactList;
            }
        ).forEach(contactListsList::add);
        return contactListsList;
    }

    public ContactList findOne(UUID id) {
        return mapper.get(id);
    }

    public ContactList save(ContactList contactList) {
        if (contactList.getId() == null) {
            contactList.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<ContactList>> violations = validator.validate(contactList);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(contactList);
        return contactList;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
