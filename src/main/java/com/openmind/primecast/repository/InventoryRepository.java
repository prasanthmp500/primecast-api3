package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Inventory;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Inventory entity.
 */
@Repository
public class InventoryRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Inventory> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public InventoryRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Inventory.class);
        this.findAllStmt = session.prepare("SELECT * FROM inventory");
        this.truncateStmt = session.prepare("TRUNCATE inventory");
    }

    public List<Inventory> findAll() {
        List<Inventory> inventoriesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Inventory inventory = new Inventory();
                inventory.setId(row.getUUID("id"));
                inventory.setName(row.getString("name"));
                inventory.setType(row.getString("type"));
                inventory.setChannel(row.getInt("channel"));
                return inventory;
            }
        ).forEach(inventoriesList::add);
        return inventoriesList;
    }

    public Inventory findOne(UUID id) {
        return mapper.get(id);
    }

    public Inventory save(Inventory inventory) {
        if (inventory.getId() == null) {
            inventory.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Inventory>> violations = validator.validate(inventory);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(inventory);
        return inventory;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
