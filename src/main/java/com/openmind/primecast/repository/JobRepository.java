package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Job;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Job entity.
 */
@Repository
public class JobRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Job> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public JobRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Job.class);
        this.findAllStmt = session.prepare("SELECT * FROM job");
        this.truncateStmt = session.prepare("TRUNCATE job");
    }

    public List<Job> findAll() {
        List<Job> jobsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Job job = new Job();
                job.setId(row.getUUID("id"));
                job.setCampaign(row.getUUID("campaign"));
                job.setCreatedBy(row.getUUID("createdBy"));
                job.setStatus(row.getUUID("status"));
                job.setContactlist(row.getUUID("contactlist"));
                job.setProfile(row.getUUID("profile"));
                job.setPersonna(row.getUUID("personna"));
                job.setRecipientGatheringList(row.getUUID("recipientGatheringList"));
                job.setLocation(row.getUUID("location"));
                job.setInventory(row.getUUID("inventory"));
                job.setStartTimestamp(row.get("startTimestamp", Instant.class));
                job.setEndTimestamp(row.get("endTimestamp", Instant.class));
                job.setDeferredStartDate(row.get("deferredStartDate", Instant.class));
                job.setEndDate(row.get("endDate", Instant.class));
                job.setRelativeValidityPeriod(row.getBool("relativeValidityPeriod"));
                job.setValidityPeriod(row.get("validityPeriod", Instant.class));
                job.setImpressionLimit(row.getInt("impressionLimit"));
                return job;
            }
        ).forEach(jobsList::add);
        return jobsList;
    }

    public Job findOne(UUID id) {
        return mapper.get(id);
    }

    public Job save(Job job) {
        if (job.getId() == null) {
            job.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Job>> violations = validator.validate(job);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(job);
        return job;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
