package com.openmind.primecast.repository;

import com.openmind.primecast.domain.CampaignType;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the CampaignType entity.
 */
@Repository
public class CampaignTypeRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<CampaignType> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public CampaignTypeRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(CampaignType.class);
        this.findAllStmt = session.prepare("SELECT * FROM campaignType");
        this.truncateStmt = session.prepare("TRUNCATE campaignType");
    }

    public List<CampaignType> findAll() {
        List<CampaignType> campaignTypesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                CampaignType campaignType = new CampaignType();
                campaignType.setId(row.getUUID("id"));
                campaignType.setName(row.getString("name"));
                campaignType.setMarketing(row.getBool("marketing"));
                return campaignType;
            }
        ).forEach(campaignTypesList::add);
        return campaignTypesList;
    }

    public CampaignType findOne(UUID id) {
        return mapper.get(id);
    }

    public CampaignType save(CampaignType campaignType) {
        if (campaignType.getId() == null) {
            campaignType.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<CampaignType>> violations = validator.validate(campaignType);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(campaignType);
        return campaignType;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
