package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Language;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Language entity.
 */
@Repository
public class LanguageRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Language> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public LanguageRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Language.class);
        this.findAllStmt = session.prepare("SELECT * FROM language");
        this.truncateStmt = session.prepare("TRUNCATE language");
    }

    public List<Language> findAll() {
        List<Language> languagesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Language language = new Language();
                language.setId(row.getUUID("id"));
                language.setName(row.getString("name"));
                language.setCode(row.getString("code"));
                return language;
            }
        ).forEach(languagesList::add);
        return languagesList;
    }

    public Language findOne(UUID id) {
        return mapper.get(id);
    }

    public Language save(Language language) {
        if (language.getId() == null) {
            language.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Language>> violations = validator.validate(language);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(language);
        return language;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
