package com.openmind.primecast.repository;

import com.openmind.primecast.domain.PrimecastAccount;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the PrimecastAccount entity.
 */
@Repository
public class PrimecastAccountRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<PrimecastAccount> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public PrimecastAccountRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(PrimecastAccount.class);
        this.findAllStmt = session.prepare("SELECT * FROM primecastAccount");
        this.truncateStmt = session.prepare("TRUNCATE primecastAccount");
    }

    public List<PrimecastAccount> findAll() {
        List<PrimecastAccount> primecastAccountsList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                PrimecastAccount primecastAccount = new PrimecastAccount();
                primecastAccount.setId(row.getUUID("id"));
                primecastAccount.setName(row.getString("name"));
                primecastAccount.setAddress1(row.getString("address1"));
                primecastAccount.setAddress2(row.getString("address2"));
                primecastAccount.setAddress3(row.getString("address3"));
                primecastAccount.setAddress4(row.getString("address4"));
                primecastAccount.setCountry(row.getString("country"));
                primecastAccount.setStatus(row.getString("status"));
                primecastAccount.setInternal(row.getBool("internal"));
                primecastAccount.setCurrency(row.getUUID("currency"));
                primecastAccount.setDefaultLanguage(row.getUUID("defaultLanguage"));
                return primecastAccount;
            }
        ).forEach(primecastAccountsList::add);
        return primecastAccountsList;
    }

    public PrimecastAccount findOne(UUID id) {
        return mapper.get(id);
    }

    public PrimecastAccount save(PrimecastAccount primecastAccount) {
        if (primecastAccount.getId() == null) {
            primecastAccount.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<PrimecastAccount>> violations = validator.validate(primecastAccount);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(primecastAccount);
        return primecastAccount;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
