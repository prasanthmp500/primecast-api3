package com.openmind.primecast.repository;

import com.openmind.primecast.domain.FlightStatus;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the FlightStatus entity.
 */
@Repository
public class FlightStatusRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<FlightStatus> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public FlightStatusRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(FlightStatus.class);
        this.findAllStmt = session.prepare("SELECT * FROM flightStatus");
        this.truncateStmt = session.prepare("TRUNCATE flightStatus");
    }

    public List<FlightStatus> findAll() {
        List<FlightStatus> flightStatusesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                FlightStatus flightStatus = new FlightStatus();
                flightStatus.setId(row.getUUID("id"));
                flightStatus.setName(row.getString("name"));
                return flightStatus;
            }
        ).forEach(flightStatusesList::add);
        return flightStatusesList;
    }

    public FlightStatus findOne(UUID id) {
        return mapper.get(id);
    }

    public FlightStatus save(FlightStatus flightStatus) {
        if (flightStatus.getId() == null) {
            flightStatus.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<FlightStatus>> violations = validator.validate(flightStatus);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(flightStatus);
        return flightStatus;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
