package com.openmind.primecast.repository;

import com.openmind.primecast.domain.PrimecastUser;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the PrimecastUser entity.
 */
@Repository
public class PrimecastUserRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<PrimecastUser> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public PrimecastUserRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(PrimecastUser.class);
        this.findAllStmt = session.prepare("SELECT * FROM primecastUser");
        this.truncateStmt = session.prepare("TRUNCATE primecastUser");
    }

    public List<PrimecastUser> findAll() {
        List<PrimecastUser> primecastUsersList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                PrimecastUser primecastUser = new PrimecastUser();
                primecastUser.setId(row.getUUID("id"));
                primecastUser.setUser(row.getUUID("user"));
                primecastUser.setAccount(row.getUUID("account"));
                primecastUser.setLanguage(row.getUUID("language"));
                return primecastUser;
            }
        ).forEach(primecastUsersList::add);
        return primecastUsersList;
    }

    public PrimecastUser findOne(UUID id) {
        return mapper.get(id);
    }

    public PrimecastUser save(PrimecastUser primecastUser) {
        if (primecastUser.getId() == null) {
            primecastUser.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<PrimecastUser>> violations = validator.validate(primecastUser);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(primecastUser);
        return primecastUser;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
