package com.openmind.primecast.repository;

import com.openmind.primecast.domain.Template;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the Template entity.
 */
@Repository
public class TemplateRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<Template> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public TemplateRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(Template.class);
        this.findAllStmt = session.prepare("SELECT * FROM template");
        this.truncateStmt = session.prepare("TRUNCATE template");
    }

    public List<Template> findAll() {
        List<Template> templatesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                Template template = new Template();
                template.setId(row.getUUID("id"));
                template.setText(row.getString("text"));
                template.setDefaultTemplate(row.getBool("defaultTemplate"));
                template.setClickThrough(row.getString("clickThrough"));
                template.setJob(row.getUUID("job"));
                template.setLanguage(row.getUUID("language"));
                return template;
            }
        ).forEach(templatesList::add);
        return templatesList;
    }

    public Template findOne(UUID id) {
        return mapper.get(id);
    }

    public Template save(Template template) {
        if (template.getId() == null) {
            template.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<Template>> violations = validator.validate(template);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(template);
        return template;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
