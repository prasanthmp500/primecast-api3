package com.openmind.primecast.repository;

import com.openmind.primecast.domain.CampaignStatus;
import org.springframework.stereotype.Repository;

import com.datastax.driver.core.*;
import com.datastax.driver.mapping.Mapper;
import com.datastax.driver.mapping.MappingManager;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.UUID;

/**
 * Cassandra repository for the CampaignStatus entity.
 */
@Repository
public class CampaignStatusRepository {

    private final Session session;

    private final Validator validator;

    private Mapper<CampaignStatus> mapper;

    private PreparedStatement findAllStmt;

    private PreparedStatement truncateStmt;

    public CampaignStatusRepository(Session session, Validator validator) {
        this.session = session;
        this.validator = validator;
        this.mapper = new MappingManager(session).mapper(CampaignStatus.class);
        this.findAllStmt = session.prepare("SELECT * FROM campaignStatus");
        this.truncateStmt = session.prepare("TRUNCATE campaignStatus");
    }

    public List<CampaignStatus> findAll() {
        List<CampaignStatus> campaignStatusesList = new ArrayList<>();
        BoundStatement stmt = findAllStmt.bind();
        session.execute(stmt).all().stream().map(
            row -> {
                CampaignStatus campaignStatus = new CampaignStatus();
                campaignStatus.setId(row.getUUID("id"));
                campaignStatus.setName(row.getString("name"));
                return campaignStatus;
            }
        ).forEach(campaignStatusesList::add);
        return campaignStatusesList;
    }

    public CampaignStatus findOne(UUID id) {
        return mapper.get(id);
    }

    public CampaignStatus save(CampaignStatus campaignStatus) {
        if (campaignStatus.getId() == null) {
            campaignStatus.setId(UUID.randomUUID());
        }
        Set<ConstraintViolation<CampaignStatus>> violations = validator.validate(campaignStatus);
        if (violations != null && !violations.isEmpty()) {
            throw new ConstraintViolationException(violations);
        }
        mapper.save(campaignStatus);
        return campaignStatus;
    }

    public void delete(UUID id) {
        mapper.delete(id);
    }

    public void deleteAll() {
        BoundStatement stmt = truncateStmt.bind();
        session.execute(stmt);
    }
}
