/**
 * Data Access Objects used by WebSocket services.
 */
package com.openmind.primecast.web.websocket.dto;
