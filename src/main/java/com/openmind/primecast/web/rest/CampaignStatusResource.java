package com.openmind.primecast.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.openmind.primecast.service.CampaignStatusService;
import com.openmind.primecast.web.rest.errors.BadRequestAlertException;
import com.openmind.primecast.web.rest.util.HeaderUtil;
import com.openmind.primecast.service.dto.CampaignStatusDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing CampaignStatus.
 */
@RestController
@RequestMapping("/api")
public class CampaignStatusResource {

    private final Logger log = LoggerFactory.getLogger(CampaignStatusResource.class);

    private static final String ENTITY_NAME = "campaignStatus";

    private final CampaignStatusService campaignStatusService;

    public CampaignStatusResource(CampaignStatusService campaignStatusService) {
        this.campaignStatusService = campaignStatusService;
    }

    /**
     * POST  /campaign-statuses : Create a new campaignStatus.
     *
     * @param campaignStatusDTO the campaignStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignStatusDTO, or with status 400 (Bad Request) if the campaignStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-statuses")
    @Timed
    public ResponseEntity<CampaignStatusDTO> createCampaignStatus(@Valid @RequestBody CampaignStatusDTO campaignStatusDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignStatus : {}", campaignStatusDTO);
        if (campaignStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignStatusDTO result = campaignStatusService.save(campaignStatusDTO);
        return ResponseEntity.created(new URI("/api/campaign-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-statuses : Updates an existing campaignStatus.
     *
     * @param campaignStatusDTO the campaignStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignStatusDTO,
     * or with status 400 (Bad Request) if the campaignStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-statuses")
    @Timed
    public ResponseEntity<CampaignStatusDTO> updateCampaignStatus(@Valid @RequestBody CampaignStatusDTO campaignStatusDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignStatus : {}", campaignStatusDTO);
        if (campaignStatusDTO.getId() == null) {
            return createCampaignStatus(campaignStatusDTO);
        }
        CampaignStatusDTO result = campaignStatusService.save(campaignStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-statuses : get all the campaignStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of campaignStatuses in body
     */
    @GetMapping("/campaign-statuses")
    @Timed
    public List<CampaignStatusDTO> getAllCampaignStatuses() {
        log.debug("REST request to get all CampaignStatuses");
        return campaignStatusService.findAll();
        }

    /**
     * GET  /campaign-statuses/:id : get the "id" campaignStatus.
     *
     * @param id the id of the campaignStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-statuses/{id}")
    @Timed
    public ResponseEntity<CampaignStatusDTO> getCampaignStatus(@PathVariable String id) {
        log.debug("REST request to get CampaignStatus : {}", id);
        CampaignStatusDTO campaignStatusDTO = campaignStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignStatusDTO));
    }

    /**
     * DELETE  /campaign-statuses/:id : delete the "id" campaignStatus.
     *
     * @param id the id of the campaignStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignStatus(@PathVariable String id) {
        log.debug("REST request to delete CampaignStatus : {}", id);
        campaignStatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
