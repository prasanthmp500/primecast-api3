package com.openmind.primecast.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.openmind.primecast.service.CampaignTypeService;
import com.openmind.primecast.web.rest.errors.BadRequestAlertException;
import com.openmind.primecast.web.rest.util.HeaderUtil;
import com.openmind.primecast.service.dto.CampaignTypeDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing CampaignType.
 */
@RestController
@RequestMapping("/api")
public class CampaignTypeResource {

    private final Logger log = LoggerFactory.getLogger(CampaignTypeResource.class);

    private static final String ENTITY_NAME = "campaignType";

    private final CampaignTypeService campaignTypeService;

    public CampaignTypeResource(CampaignTypeService campaignTypeService) {
        this.campaignTypeService = campaignTypeService;
    }

    /**
     * POST  /campaign-types : Create a new campaignType.
     *
     * @param campaignTypeDTO the campaignTypeDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new campaignTypeDTO, or with status 400 (Bad Request) if the campaignType has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/campaign-types")
    @Timed
    public ResponseEntity<CampaignTypeDTO> createCampaignType(@Valid @RequestBody CampaignTypeDTO campaignTypeDTO) throws URISyntaxException {
        log.debug("REST request to save CampaignType : {}", campaignTypeDTO);
        if (campaignTypeDTO.getId() != null) {
            throw new BadRequestAlertException("A new campaignType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        CampaignTypeDTO result = campaignTypeService.save(campaignTypeDTO);
        return ResponseEntity.created(new URI("/api/campaign-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /campaign-types : Updates an existing campaignType.
     *
     * @param campaignTypeDTO the campaignTypeDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated campaignTypeDTO,
     * or with status 400 (Bad Request) if the campaignTypeDTO is not valid,
     * or with status 500 (Internal Server Error) if the campaignTypeDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/campaign-types")
    @Timed
    public ResponseEntity<CampaignTypeDTO> updateCampaignType(@Valid @RequestBody CampaignTypeDTO campaignTypeDTO) throws URISyntaxException {
        log.debug("REST request to update CampaignType : {}", campaignTypeDTO);
        if (campaignTypeDTO.getId() == null) {
            return createCampaignType(campaignTypeDTO);
        }
        CampaignTypeDTO result = campaignTypeService.save(campaignTypeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, campaignTypeDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /campaign-types : get all the campaignTypes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of campaignTypes in body
     */
    @GetMapping("/campaign-types")
    @Timed
    public List<CampaignTypeDTO> getAllCampaignTypes() {
        log.debug("REST request to get all CampaignTypes");
        return campaignTypeService.findAll();
        }

    /**
     * GET  /campaign-types/:id : get the "id" campaignType.
     *
     * @param id the id of the campaignTypeDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the campaignTypeDTO, or with status 404 (Not Found)
     */
    @GetMapping("/campaign-types/{id}")
    @Timed
    public ResponseEntity<CampaignTypeDTO> getCampaignType(@PathVariable String id) {
        log.debug("REST request to get CampaignType : {}", id);
        CampaignTypeDTO campaignTypeDTO = campaignTypeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(campaignTypeDTO));
    }

    /**
     * DELETE  /campaign-types/:id : delete the "id" campaignType.
     *
     * @param id the id of the campaignTypeDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/campaign-types/{id}")
    @Timed
    public ResponseEntity<Void> deleteCampaignType(@PathVariable String id) {
        log.debug("REST request to delete CampaignType : {}", id);
        campaignTypeService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
