/**
 * View Models used by Spring MVC REST controllers.
 */
package com.openmind.primecast.web.rest.vm;
