package com.openmind.primecast.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.openmind.primecast.service.FlightStatusService;
import com.openmind.primecast.web.rest.errors.BadRequestAlertException;
import com.openmind.primecast.web.rest.util.HeaderUtil;
import com.openmind.primecast.service.dto.FlightStatusDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing FlightStatus.
 */
@RestController
@RequestMapping("/api")
public class FlightStatusResource {

    private final Logger log = LoggerFactory.getLogger(FlightStatusResource.class);

    private static final String ENTITY_NAME = "flightStatus";

    private final FlightStatusService flightStatusService;

    public FlightStatusResource(FlightStatusService flightStatusService) {
        this.flightStatusService = flightStatusService;
    }

    /**
     * POST  /flight-statuses : Create a new flightStatus.
     *
     * @param flightStatusDTO the flightStatusDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new flightStatusDTO, or with status 400 (Bad Request) if the flightStatus has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/flight-statuses")
    @Timed
    public ResponseEntity<FlightStatusDTO> createFlightStatus(@Valid @RequestBody FlightStatusDTO flightStatusDTO) throws URISyntaxException {
        log.debug("REST request to save FlightStatus : {}", flightStatusDTO);
        if (flightStatusDTO.getId() != null) {
            throw new BadRequestAlertException("A new flightStatus cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FlightStatusDTO result = flightStatusService.save(flightStatusDTO);
        return ResponseEntity.created(new URI("/api/flight-statuses/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /flight-statuses : Updates an existing flightStatus.
     *
     * @param flightStatusDTO the flightStatusDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated flightStatusDTO,
     * or with status 400 (Bad Request) if the flightStatusDTO is not valid,
     * or with status 500 (Internal Server Error) if the flightStatusDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/flight-statuses")
    @Timed
    public ResponseEntity<FlightStatusDTO> updateFlightStatus(@Valid @RequestBody FlightStatusDTO flightStatusDTO) throws URISyntaxException {
        log.debug("REST request to update FlightStatus : {}", flightStatusDTO);
        if (flightStatusDTO.getId() == null) {
            return createFlightStatus(flightStatusDTO);
        }
        FlightStatusDTO result = flightStatusService.save(flightStatusDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, flightStatusDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /flight-statuses : get all the flightStatuses.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of flightStatuses in body
     */
    @GetMapping("/flight-statuses")
    @Timed
    public List<FlightStatusDTO> getAllFlightStatuses() {
        log.debug("REST request to get all FlightStatuses");
        return flightStatusService.findAll();
        }

    /**
     * GET  /flight-statuses/:id : get the "id" flightStatus.
     *
     * @param id the id of the flightStatusDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the flightStatusDTO, or with status 404 (Not Found)
     */
    @GetMapping("/flight-statuses/{id}")
    @Timed
    public ResponseEntity<FlightStatusDTO> getFlightStatus(@PathVariable String id) {
        log.debug("REST request to get FlightStatus : {}", id);
        FlightStatusDTO flightStatusDTO = flightStatusService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(flightStatusDTO));
    }

    /**
     * DELETE  /flight-statuses/:id : delete the "id" flightStatus.
     *
     * @param id the id of the flightStatusDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/flight-statuses/{id}")
    @Timed
    public ResponseEntity<Void> deleteFlightStatus(@PathVariable String id) {
        log.debug("REST request to delete FlightStatus : {}", id);
        flightStatusService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
