package com.openmind.primecast.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.openmind.primecast.service.PrimecastUserService;
import com.openmind.primecast.web.rest.errors.BadRequestAlertException;
import com.openmind.primecast.web.rest.util.HeaderUtil;
import com.openmind.primecast.service.dto.PrimecastUserDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing PrimecastUser.
 */
@RestController
@RequestMapping("/api")
public class PrimecastUserResource {

    private final Logger log = LoggerFactory.getLogger(PrimecastUserResource.class);

    private static final String ENTITY_NAME = "primecastUser";

    private final PrimecastUserService primecastUserService;

    public PrimecastUserResource(PrimecastUserService primecastUserService) {
        this.primecastUserService = primecastUserService;
    }

    /**
     * POST  /primecast-users : Create a new primecastUser.
     *
     * @param primecastUserDTO the primecastUserDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new primecastUserDTO, or with status 400 (Bad Request) if the primecastUser has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/primecast-users")
    @Timed
    public ResponseEntity<PrimecastUserDTO> createPrimecastUser(@Valid @RequestBody PrimecastUserDTO primecastUserDTO) throws URISyntaxException {
        log.debug("REST request to save PrimecastUser : {}", primecastUserDTO);
        if (primecastUserDTO.getId() != null) {
            throw new BadRequestAlertException("A new primecastUser cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrimecastUserDTO result = primecastUserService.save(primecastUserDTO);
        return ResponseEntity.created(new URI("/api/primecast-users/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /primecast-users : Updates an existing primecastUser.
     *
     * @param primecastUserDTO the primecastUserDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated primecastUserDTO,
     * or with status 400 (Bad Request) if the primecastUserDTO is not valid,
     * or with status 500 (Internal Server Error) if the primecastUserDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/primecast-users")
    @Timed
    public ResponseEntity<PrimecastUserDTO> updatePrimecastUser(@Valid @RequestBody PrimecastUserDTO primecastUserDTO) throws URISyntaxException {
        log.debug("REST request to update PrimecastUser : {}", primecastUserDTO);
        if (primecastUserDTO.getId() == null) {
            return createPrimecastUser(primecastUserDTO);
        }
        PrimecastUserDTO result = primecastUserService.save(primecastUserDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, primecastUserDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /primecast-users : get all the primecastUsers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of primecastUsers in body
     */
    @GetMapping("/primecast-users")
    @Timed
    public List<PrimecastUserDTO> getAllPrimecastUsers() {
        log.debug("REST request to get all PrimecastUsers");
        return primecastUserService.findAll();
        }

    /**
     * GET  /primecast-users/:id : get the "id" primecastUser.
     *
     * @param id the id of the primecastUserDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the primecastUserDTO, or with status 404 (Not Found)
     */
    @GetMapping("/primecast-users/{id}")
    @Timed
    public ResponseEntity<PrimecastUserDTO> getPrimecastUser(@PathVariable String id) {
        log.debug("REST request to get PrimecastUser : {}", id);
        PrimecastUserDTO primecastUserDTO = primecastUserService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(primecastUserDTO));
    }

    /**
     * DELETE  /primecast-users/:id : delete the "id" primecastUser.
     *
     * @param id the id of the primecastUserDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/primecast-users/{id}")
    @Timed
    public ResponseEntity<Void> deletePrimecastUser(@PathVariable String id) {
        log.debug("REST request to delete PrimecastUser : {}", id);
        primecastUserService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
