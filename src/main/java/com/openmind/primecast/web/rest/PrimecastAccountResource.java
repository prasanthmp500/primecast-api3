package com.openmind.primecast.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.openmind.primecast.service.PrimecastAccountService;
import com.openmind.primecast.web.rest.errors.BadRequestAlertException;
import com.openmind.primecast.web.rest.util.HeaderUtil;
import com.openmind.primecast.service.dto.PrimecastAccountDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * REST controller for managing PrimecastAccount.
 */
@RestController
@RequestMapping("/api")
public class PrimecastAccountResource {

    private final Logger log = LoggerFactory.getLogger(PrimecastAccountResource.class);

    private static final String ENTITY_NAME = "primecastAccount";

    private final PrimecastAccountService primecastAccountService;

    public PrimecastAccountResource(PrimecastAccountService primecastAccountService) {
        this.primecastAccountService = primecastAccountService;
    }

    /**
     * POST  /primecast-accounts : Create a new primecastAccount.
     *
     * @param primecastAccountDTO the primecastAccountDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new primecastAccountDTO, or with status 400 (Bad Request) if the primecastAccount has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/primecast-accounts")
    @Timed
    public ResponseEntity<PrimecastAccountDTO> createPrimecastAccount(@Valid @RequestBody PrimecastAccountDTO primecastAccountDTO) throws URISyntaxException {
        log.debug("REST request to save PrimecastAccount : {}", primecastAccountDTO);
        if (primecastAccountDTO.getId() != null) {
            throw new BadRequestAlertException("A new primecastAccount cannot already have an ID", ENTITY_NAME, "idexists");
        }
        PrimecastAccountDTO result = primecastAccountService.save(primecastAccountDTO);
        return ResponseEntity.created(new URI("/api/primecast-accounts/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /primecast-accounts : Updates an existing primecastAccount.
     *
     * @param primecastAccountDTO the primecastAccountDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated primecastAccountDTO,
     * or with status 400 (Bad Request) if the primecastAccountDTO is not valid,
     * or with status 500 (Internal Server Error) if the primecastAccountDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/primecast-accounts")
    @Timed
    public ResponseEntity<PrimecastAccountDTO> updatePrimecastAccount(@Valid @RequestBody PrimecastAccountDTO primecastAccountDTO) throws URISyntaxException {
        log.debug("REST request to update PrimecastAccount : {}", primecastAccountDTO);
        if (primecastAccountDTO.getId() == null) {
            return createPrimecastAccount(primecastAccountDTO);
        }
        PrimecastAccountDTO result = primecastAccountService.save(primecastAccountDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, primecastAccountDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /primecast-accounts : get all the primecastAccounts.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of primecastAccounts in body
     */
    @GetMapping("/primecast-accounts")
    @Timed
    public List<PrimecastAccountDTO> getAllPrimecastAccounts() {
        log.debug("REST request to get all PrimecastAccounts");
        return primecastAccountService.findAll();
        }

    /**
     * GET  /primecast-accounts/:id : get the "id" primecastAccount.
     *
     * @param id the id of the primecastAccountDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the primecastAccountDTO, or with status 404 (Not Found)
     */
    @GetMapping("/primecast-accounts/{id}")
    @Timed
    public ResponseEntity<PrimecastAccountDTO> getPrimecastAccount(@PathVariable String id) {
        log.debug("REST request to get PrimecastAccount : {}", id);
        PrimecastAccountDTO primecastAccountDTO = primecastAccountService.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(primecastAccountDTO));
    }

    /**
     * DELETE  /primecast-accounts/:id : delete the "id" primecastAccount.
     *
     * @param id the id of the primecastAccountDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/primecast-accounts/{id}")
    @Timed
    public ResponseEntity<Void> deletePrimecastAccount(@PathVariable String id) {
        log.debug("REST request to delete PrimecastAccount : {}", id);
        primecastAccountService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id)).build();
    }
}
