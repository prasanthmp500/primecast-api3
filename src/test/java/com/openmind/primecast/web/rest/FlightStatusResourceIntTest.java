package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.FlightStatus;
import com.openmind.primecast.repository.FlightStatusRepository;
import com.openmind.primecast.service.FlightStatusService;
import com.openmind.primecast.service.dto.FlightStatusDTO;
import com.openmind.primecast.service.mapper.FlightStatusMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the FlightStatusResource REST controller.
 *
 * @see FlightStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class FlightStatusResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private FlightStatusRepository flightStatusRepository;

    @Autowired
    private FlightStatusMapper flightStatusMapper;

    @Autowired
    private FlightStatusService flightStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restFlightStatusMockMvc;

    private FlightStatus flightStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FlightStatusResource flightStatusResource = new FlightStatusResource(flightStatusService);
        this.restFlightStatusMockMvc = MockMvcBuilders.standaloneSetup(flightStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FlightStatus createEntity() {
        FlightStatus flightStatus = new FlightStatus()
            .name(DEFAULT_NAME);
        return flightStatus;
    }

    @Before
    public void initTest() {
        flightStatusRepository.deleteAll();
        flightStatus = createEntity();
    }

    @Test
    public void createFlightStatus() throws Exception {
        int databaseSizeBeforeCreate = flightStatusRepository.findAll().size();

        // Create the FlightStatus
        FlightStatusDTO flightStatusDTO = flightStatusMapper.toDto(flightStatus);
        restFlightStatusMockMvc.perform(post("/api/flight-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flightStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the FlightStatus in the database
        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeCreate + 1);
        FlightStatus testFlightStatus = flightStatusList.get(flightStatusList.size() - 1);
        assertThat(testFlightStatus.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    public void createFlightStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = flightStatusRepository.findAll().size();

        // Create the FlightStatus with an existing ID
        flightStatus.setId(UUID.randomUUID());
        FlightStatusDTO flightStatusDTO = flightStatusMapper.toDto(flightStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFlightStatusMockMvc.perform(post("/api/flight-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flightStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FlightStatus in the database
        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = flightStatusRepository.findAll().size();
        // set the field null
        flightStatus.setName(null);

        // Create the FlightStatus, which fails.
        FlightStatusDTO flightStatusDTO = flightStatusMapper.toDto(flightStatus);

        restFlightStatusMockMvc.perform(post("/api/flight-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flightStatusDTO)))
            .andExpect(status().isBadRequest());

        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllFlightStatuses() throws Exception {
        // Initialize the database
        flightStatusRepository.save(flightStatus);

        // Get all the flightStatusList
        restFlightStatusMockMvc.perform(get("/api/flight-statuses"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(flightStatus.getId().toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    public void getFlightStatus() throws Exception {
        // Initialize the database
        flightStatusRepository.save(flightStatus);

        // Get the flightStatus
        restFlightStatusMockMvc.perform(get("/api/flight-statuses/{id}", flightStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(flightStatus.getId().toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    public void getNonExistingFlightStatus() throws Exception {
        // Get the flightStatus
        restFlightStatusMockMvc.perform(get("/api/flight-statuses/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateFlightStatus() throws Exception {
        // Initialize the database
        flightStatusRepository.save(flightStatus);
        int databaseSizeBeforeUpdate = flightStatusRepository.findAll().size();

        // Update the flightStatus
        FlightStatus updatedFlightStatus = flightStatusRepository.findOne(flightStatus.getId());
        updatedFlightStatus
            .name(UPDATED_NAME);
        FlightStatusDTO flightStatusDTO = flightStatusMapper.toDto(updatedFlightStatus);

        restFlightStatusMockMvc.perform(put("/api/flight-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flightStatusDTO)))
            .andExpect(status().isOk());

        // Validate the FlightStatus in the database
        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeUpdate);
        FlightStatus testFlightStatus = flightStatusList.get(flightStatusList.size() - 1);
        assertThat(testFlightStatus.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    public void updateNonExistingFlightStatus() throws Exception {
        int databaseSizeBeforeUpdate = flightStatusRepository.findAll().size();

        // Create the FlightStatus
        FlightStatusDTO flightStatusDTO = flightStatusMapper.toDto(flightStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFlightStatusMockMvc.perform(put("/api/flight-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(flightStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the FlightStatus in the database
        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteFlightStatus() throws Exception {
        // Initialize the database
        flightStatusRepository.save(flightStatus);
        int databaseSizeBeforeDelete = flightStatusRepository.findAll().size();

        // Get the flightStatus
        restFlightStatusMockMvc.perform(delete("/api/flight-statuses/{id}", flightStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FlightStatus> flightStatusList = flightStatusRepository.findAll();
        assertThat(flightStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FlightStatus.class);
        FlightStatus flightStatus1 = new FlightStatus();
        flightStatus1.setId(UUID.randomUUID());
        FlightStatus flightStatus2 = new FlightStatus();
        flightStatus2.setId(flightStatus1.getId());
        assertThat(flightStatus1).isEqualTo(flightStatus2);
        flightStatus2.setId(UUID.randomUUID());
        assertThat(flightStatus1).isNotEqualTo(flightStatus2);
        flightStatus1.setId(null);
        assertThat(flightStatus1).isNotEqualTo(flightStatus2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(FlightStatusDTO.class);
        FlightStatusDTO flightStatusDTO1 = new FlightStatusDTO();
        flightStatusDTO1.setId(UUID.randomUUID());
        FlightStatusDTO flightStatusDTO2 = new FlightStatusDTO();
        assertThat(flightStatusDTO1).isNotEqualTo(flightStatusDTO2);
        flightStatusDTO2.setId(flightStatusDTO1.getId());
        assertThat(flightStatusDTO1).isEqualTo(flightStatusDTO2);
        flightStatusDTO2.setId(UUID.randomUUID());
        assertThat(flightStatusDTO1).isNotEqualTo(flightStatusDTO2);
        flightStatusDTO1.setId(null);
        assertThat(flightStatusDTO1).isNotEqualTo(flightStatusDTO2);
    }
}
