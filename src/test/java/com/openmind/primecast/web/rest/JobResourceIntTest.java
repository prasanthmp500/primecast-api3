package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.Job;
import com.openmind.primecast.repository.JobRepository;
import com.openmind.primecast.service.JobService;
import com.openmind.primecast.service.dto.JobDTO;
import com.openmind.primecast.service.mapper.JobMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the JobResource REST controller.
 *
 * @see JobResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class JobResourceIntTest extends AbstractCassandraTest {

    private static final UUID DEFAULT_CAMPAIGN = UUID.randomUUID();
    private static final UUID UPDATED_CAMPAIGN = UUID.randomUUID();

    private static final UUID DEFAULT_CREATED_BY = UUID.randomUUID();
    private static final UUID UPDATED_CREATED_BY = UUID.randomUUID();

    private static final UUID DEFAULT_STATUS = UUID.randomUUID();
    private static final UUID UPDATED_STATUS = UUID.randomUUID();

    private static final UUID DEFAULT_CONTACTLIST = UUID.randomUUID();
    private static final UUID UPDATED_CONTACTLIST = UUID.randomUUID();

    private static final UUID DEFAULT_PROFILE = UUID.randomUUID();
    private static final UUID UPDATED_PROFILE = UUID.randomUUID();

    private static final UUID DEFAULT_PERSONNA = UUID.randomUUID();
    private static final UUID UPDATED_PERSONNA = UUID.randomUUID();

    private static final UUID DEFAULT_RECIPIENT_GATHERING_LIST = UUID.randomUUID();
    private static final UUID UPDATED_RECIPIENT_GATHERING_LIST = UUID.randomUUID();

    private static final UUID DEFAULT_LOCATION = UUID.randomUUID();
    private static final UUID UPDATED_LOCATION = UUID.randomUUID();

    private static final UUID DEFAULT_INVENTORY = UUID.randomUUID();
    private static final UUID UPDATED_INVENTORY = UUID.randomUUID();

    private static final Instant DEFAULT_START_TIMESTAMP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_TIMESTAMP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIMESTAMP = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIMESTAMP = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_DEFERRED_START_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DEFERRED_START_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Boolean DEFAULT_RELATIVE_VALIDITY_PERIOD = false;
    private static final Boolean UPDATED_RELATIVE_VALIDITY_PERIOD = true;

    private static final Instant DEFAULT_VALIDITY_PERIOD = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_VALIDITY_PERIOD = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Integer DEFAULT_IMPRESSION_LIMIT = 1;
    private static final Integer UPDATED_IMPRESSION_LIMIT = 2;

    @Autowired
    private JobRepository jobRepository;

    @Autowired
    private JobMapper jobMapper;

    @Autowired
    private JobService jobService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restJobMockMvc;

    private Job job;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final JobResource jobResource = new JobResource(jobService);
        this.restJobMockMvc = MockMvcBuilders.standaloneSetup(jobResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Job createEntity() {
        Job job = new Job()
            .campaign(DEFAULT_CAMPAIGN)
            .createdBy(DEFAULT_CREATED_BY)
            .status(DEFAULT_STATUS)
            .contactlist(DEFAULT_CONTACTLIST)
            .profile(DEFAULT_PROFILE)
            .personna(DEFAULT_PERSONNA)
            .recipientGatheringList(DEFAULT_RECIPIENT_GATHERING_LIST)
            .location(DEFAULT_LOCATION)
            .inventory(DEFAULT_INVENTORY)
            .startTimestamp(DEFAULT_START_TIMESTAMP)
            .endTimestamp(DEFAULT_END_TIMESTAMP)
            .deferredStartDate(DEFAULT_DEFERRED_START_DATE)
            .endDate(DEFAULT_END_DATE)
            .relativeValidityPeriod(DEFAULT_RELATIVE_VALIDITY_PERIOD)
            .validityPeriod(DEFAULT_VALIDITY_PERIOD)
            .impressionLimit(DEFAULT_IMPRESSION_LIMIT);
        return job;
    }

    @Before
    public void initTest() {
        jobRepository.deleteAll();
        job = createEntity();
    }

    @Test
    public void createJob() throws Exception {
        int databaseSizeBeforeCreate = jobRepository.findAll().size();

        // Create the Job
        JobDTO jobDTO = jobMapper.toDto(job);
        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isCreated());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeCreate + 1);
        Job testJob = jobList.get(jobList.size() - 1);
        assertThat(testJob.getCampaign()).isEqualTo(DEFAULT_CAMPAIGN);
        assertThat(testJob.getCreatedBy()).isEqualTo(DEFAULT_CREATED_BY);
        assertThat(testJob.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testJob.getContactlist()).isEqualTo(DEFAULT_CONTACTLIST);
        assertThat(testJob.getProfile()).isEqualTo(DEFAULT_PROFILE);
        assertThat(testJob.getPersonna()).isEqualTo(DEFAULT_PERSONNA);
        assertThat(testJob.getRecipientGatheringList()).isEqualTo(DEFAULT_RECIPIENT_GATHERING_LIST);
        assertThat(testJob.getLocation()).isEqualTo(DEFAULT_LOCATION);
        assertThat(testJob.getInventory()).isEqualTo(DEFAULT_INVENTORY);
        assertThat(testJob.getStartTimestamp()).isEqualTo(DEFAULT_START_TIMESTAMP);
        assertThat(testJob.getEndTimestamp()).isEqualTo(DEFAULT_END_TIMESTAMP);
        assertThat(testJob.getDeferredStartDate()).isEqualTo(DEFAULT_DEFERRED_START_DATE);
        assertThat(testJob.getEndDate()).isEqualTo(DEFAULT_END_DATE);
        assertThat(testJob.isRelativeValidityPeriod()).isEqualTo(DEFAULT_RELATIVE_VALIDITY_PERIOD);
        assertThat(testJob.getValidityPeriod()).isEqualTo(DEFAULT_VALIDITY_PERIOD);
        assertThat(testJob.getImpressionLimit()).isEqualTo(DEFAULT_IMPRESSION_LIMIT);
    }

    @Test
    public void createJobWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = jobRepository.findAll().size();

        // Create the Job with an existing ID
        job.setId(UUID.randomUUID());
        JobDTO jobDTO = jobMapper.toDto(job);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkCampaignIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setCampaign(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkCreatedByIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setCreatedBy(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setStatus(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkInventoryIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setInventory(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkStartTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setStartTimestamp(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkEndTimestampIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setEndTimestamp(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkEndDateIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setEndDate(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkImpressionLimitIsRequired() throws Exception {
        int databaseSizeBeforeTest = jobRepository.findAll().size();
        // set the field null
        job.setImpressionLimit(null);

        // Create the Job, which fails.
        JobDTO jobDTO = jobMapper.toDto(job);

        restJobMockMvc.perform(post("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isBadRequest());

        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllJobs() throws Exception {
        // Initialize the database
        jobRepository.save(job);

        // Get all the jobList
        restJobMockMvc.perform(get("/api/jobs"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(job.getId().toString())))
            .andExpect(jsonPath("$.[*].campaign").value(hasItem(DEFAULT_CAMPAIGN.toString())))
            .andExpect(jsonPath("$.[*].createdBy").value(hasItem(DEFAULT_CREATED_BY.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].contactlist").value(hasItem(DEFAULT_CONTACTLIST.toString())))
            .andExpect(jsonPath("$.[*].profile").value(hasItem(DEFAULT_PROFILE.toString())))
            .andExpect(jsonPath("$.[*].personna").value(hasItem(DEFAULT_PERSONNA.toString())))
            .andExpect(jsonPath("$.[*].recipientGatheringList").value(hasItem(DEFAULT_RECIPIENT_GATHERING_LIST.toString())))
            .andExpect(jsonPath("$.[*].location").value(hasItem(DEFAULT_LOCATION.toString())))
            .andExpect(jsonPath("$.[*].inventory").value(hasItem(DEFAULT_INVENTORY.toString())))
            .andExpect(jsonPath("$.[*].startTimestamp").value(hasItem(DEFAULT_START_TIMESTAMP.toString())))
            .andExpect(jsonPath("$.[*].endTimestamp").value(hasItem(DEFAULT_END_TIMESTAMP.toString())))
            .andExpect(jsonPath("$.[*].deferredStartDate").value(hasItem(DEFAULT_DEFERRED_START_DATE.toString())))
            .andExpect(jsonPath("$.[*].endDate").value(hasItem(DEFAULT_END_DATE.toString())))
            .andExpect(jsonPath("$.[*].relativeValidityPeriod").value(hasItem(DEFAULT_RELATIVE_VALIDITY_PERIOD.booleanValue())))
            .andExpect(jsonPath("$.[*].validityPeriod").value(hasItem(DEFAULT_VALIDITY_PERIOD.toString())))
            .andExpect(jsonPath("$.[*].impressionLimit").value(hasItem(DEFAULT_IMPRESSION_LIMIT)));
    }

    @Test
    public void getJob() throws Exception {
        // Initialize the database
        jobRepository.save(job);

        // Get the job
        restJobMockMvc.perform(get("/api/jobs/{id}", job.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(job.getId().toString()))
            .andExpect(jsonPath("$.campaign").value(DEFAULT_CAMPAIGN.toString()))
            .andExpect(jsonPath("$.createdBy").value(DEFAULT_CREATED_BY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.contactlist").value(DEFAULT_CONTACTLIST.toString()))
            .andExpect(jsonPath("$.profile").value(DEFAULT_PROFILE.toString()))
            .andExpect(jsonPath("$.personna").value(DEFAULT_PERSONNA.toString()))
            .andExpect(jsonPath("$.recipientGatheringList").value(DEFAULT_RECIPIENT_GATHERING_LIST.toString()))
            .andExpect(jsonPath("$.location").value(DEFAULT_LOCATION.toString()))
            .andExpect(jsonPath("$.inventory").value(DEFAULT_INVENTORY.toString()))
            .andExpect(jsonPath("$.startTimestamp").value(DEFAULT_START_TIMESTAMP.toString()))
            .andExpect(jsonPath("$.endTimestamp").value(DEFAULT_END_TIMESTAMP.toString()))
            .andExpect(jsonPath("$.deferredStartDate").value(DEFAULT_DEFERRED_START_DATE.toString()))
            .andExpect(jsonPath("$.endDate").value(DEFAULT_END_DATE.toString()))
            .andExpect(jsonPath("$.relativeValidityPeriod").value(DEFAULT_RELATIVE_VALIDITY_PERIOD.booleanValue()))
            .andExpect(jsonPath("$.validityPeriod").value(DEFAULT_VALIDITY_PERIOD.toString()))
            .andExpect(jsonPath("$.impressionLimit").value(DEFAULT_IMPRESSION_LIMIT));
    }

    @Test
    public void getNonExistingJob() throws Exception {
        // Get the job
        restJobMockMvc.perform(get("/api/jobs/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateJob() throws Exception {
        // Initialize the database
        jobRepository.save(job);
        int databaseSizeBeforeUpdate = jobRepository.findAll().size();

        // Update the job
        Job updatedJob = jobRepository.findOne(job.getId());
        updatedJob
            .campaign(UPDATED_CAMPAIGN)
            .createdBy(UPDATED_CREATED_BY)
            .status(UPDATED_STATUS)
            .contactlist(UPDATED_CONTACTLIST)
            .profile(UPDATED_PROFILE)
            .personna(UPDATED_PERSONNA)
            .recipientGatheringList(UPDATED_RECIPIENT_GATHERING_LIST)
            .location(UPDATED_LOCATION)
            .inventory(UPDATED_INVENTORY)
            .startTimestamp(UPDATED_START_TIMESTAMP)
            .endTimestamp(UPDATED_END_TIMESTAMP)
            .deferredStartDate(UPDATED_DEFERRED_START_DATE)
            .endDate(UPDATED_END_DATE)
            .relativeValidityPeriod(UPDATED_RELATIVE_VALIDITY_PERIOD)
            .validityPeriod(UPDATED_VALIDITY_PERIOD)
            .impressionLimit(UPDATED_IMPRESSION_LIMIT);
        JobDTO jobDTO = jobMapper.toDto(updatedJob);

        restJobMockMvc.perform(put("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isOk());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeUpdate);
        Job testJob = jobList.get(jobList.size() - 1);
        assertThat(testJob.getCampaign()).isEqualTo(UPDATED_CAMPAIGN);
        assertThat(testJob.getCreatedBy()).isEqualTo(UPDATED_CREATED_BY);
        assertThat(testJob.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testJob.getContactlist()).isEqualTo(UPDATED_CONTACTLIST);
        assertThat(testJob.getProfile()).isEqualTo(UPDATED_PROFILE);
        assertThat(testJob.getPersonna()).isEqualTo(UPDATED_PERSONNA);
        assertThat(testJob.getRecipientGatheringList()).isEqualTo(UPDATED_RECIPIENT_GATHERING_LIST);
        assertThat(testJob.getLocation()).isEqualTo(UPDATED_LOCATION);
        assertThat(testJob.getInventory()).isEqualTo(UPDATED_INVENTORY);
        assertThat(testJob.getStartTimestamp()).isEqualTo(UPDATED_START_TIMESTAMP);
        assertThat(testJob.getEndTimestamp()).isEqualTo(UPDATED_END_TIMESTAMP);
        assertThat(testJob.getDeferredStartDate()).isEqualTo(UPDATED_DEFERRED_START_DATE);
        assertThat(testJob.getEndDate()).isEqualTo(UPDATED_END_DATE);
        assertThat(testJob.isRelativeValidityPeriod()).isEqualTo(UPDATED_RELATIVE_VALIDITY_PERIOD);
        assertThat(testJob.getValidityPeriod()).isEqualTo(UPDATED_VALIDITY_PERIOD);
        assertThat(testJob.getImpressionLimit()).isEqualTo(UPDATED_IMPRESSION_LIMIT);
    }

    @Test
    public void updateNonExistingJob() throws Exception {
        int databaseSizeBeforeUpdate = jobRepository.findAll().size();

        // Create the Job
        JobDTO jobDTO = jobMapper.toDto(job);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restJobMockMvc.perform(put("/api/jobs")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(jobDTO)))
            .andExpect(status().isCreated());

        // Validate the Job in the database
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteJob() throws Exception {
        // Initialize the database
        jobRepository.save(job);
        int databaseSizeBeforeDelete = jobRepository.findAll().size();

        // Get the job
        restJobMockMvc.perform(delete("/api/jobs/{id}", job.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Job> jobList = jobRepository.findAll();
        assertThat(jobList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Job.class);
        Job job1 = new Job();
        job1.setId(UUID.randomUUID());
        Job job2 = new Job();
        job2.setId(job1.getId());
        assertThat(job1).isEqualTo(job2);
        job2.setId(UUID.randomUUID());
        assertThat(job1).isNotEqualTo(job2);
        job1.setId(null);
        assertThat(job1).isNotEqualTo(job2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JobDTO.class);
        JobDTO jobDTO1 = new JobDTO();
        jobDTO1.setId(UUID.randomUUID());
        JobDTO jobDTO2 = new JobDTO();
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
        jobDTO2.setId(jobDTO1.getId());
        assertThat(jobDTO1).isEqualTo(jobDTO2);
        jobDTO2.setId(UUID.randomUUID());
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
        jobDTO1.setId(null);
        assertThat(jobDTO1).isNotEqualTo(jobDTO2);
    }
}
