package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.CampaignType;
import com.openmind.primecast.repository.CampaignTypeRepository;
import com.openmind.primecast.service.CampaignTypeService;
import com.openmind.primecast.service.dto.CampaignTypeDTO;
import com.openmind.primecast.service.mapper.CampaignTypeMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampaignTypeResource REST controller.
 *
 * @see CampaignTypeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class CampaignTypeResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final Boolean DEFAULT_MARKETING = false;
    private static final Boolean UPDATED_MARKETING = true;

    @Autowired
    private CampaignTypeRepository campaignTypeRepository;

    @Autowired
    private CampaignTypeMapper campaignTypeMapper;

    @Autowired
    private CampaignTypeService campaignTypeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restCampaignTypeMockMvc;

    private CampaignType campaignType;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampaignTypeResource campaignTypeResource = new CampaignTypeResource(campaignTypeService);
        this.restCampaignTypeMockMvc = MockMvcBuilders.standaloneSetup(campaignTypeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CampaignType createEntity() {
        CampaignType campaignType = new CampaignType()
            .name(DEFAULT_NAME)
            .marketing(DEFAULT_MARKETING);
        return campaignType;
    }

    @Before
    public void initTest() {
        campaignTypeRepository.deleteAll();
        campaignType = createEntity();
    }

    @Test
    public void createCampaignType() throws Exception {
        int databaseSizeBeforeCreate = campaignTypeRepository.findAll().size();

        // Create the CampaignType
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(campaignType);
        restCampaignTypeMockMvc.perform(post("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignType in the database
        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeCreate + 1);
        CampaignType testCampaignType = campaignTypeList.get(campaignTypeList.size() - 1);
        assertThat(testCampaignType.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testCampaignType.isMarketing()).isEqualTo(DEFAULT_MARKETING);
    }

    @Test
    public void createCampaignTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campaignTypeRepository.findAll().size();

        // Create the CampaignType with an existing ID
        campaignType.setId(UUID.randomUUID());
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(campaignType);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampaignTypeMockMvc.perform(post("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignType in the database
        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = campaignTypeRepository.findAll().size();
        // set the field null
        campaignType.setName(null);

        // Create the CampaignType, which fails.
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(campaignType);

        restCampaignTypeMockMvc.perform(post("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isBadRequest());

        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkMarketingIsRequired() throws Exception {
        int databaseSizeBeforeTest = campaignTypeRepository.findAll().size();
        // set the field null
        campaignType.setMarketing(null);

        // Create the CampaignType, which fails.
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(campaignType);

        restCampaignTypeMockMvc.perform(post("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isBadRequest());

        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllCampaignTypes() throws Exception {
        // Initialize the database
        campaignTypeRepository.save(campaignType);

        // Get all the campaignTypeList
        restCampaignTypeMockMvc.perform(get("/api/campaign-types"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campaignType.getId().toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].marketing").value(hasItem(DEFAULT_MARKETING.booleanValue())));
    }

    @Test
    public void getCampaignType() throws Exception {
        // Initialize the database
        campaignTypeRepository.save(campaignType);

        // Get the campaignType
        restCampaignTypeMockMvc.perform(get("/api/campaign-types/{id}", campaignType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campaignType.getId().toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.marketing").value(DEFAULT_MARKETING.booleanValue()));
    }

    @Test
    public void getNonExistingCampaignType() throws Exception {
        // Get the campaignType
        restCampaignTypeMockMvc.perform(get("/api/campaign-types/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCampaignType() throws Exception {
        // Initialize the database
        campaignTypeRepository.save(campaignType);
        int databaseSizeBeforeUpdate = campaignTypeRepository.findAll().size();

        // Update the campaignType
        CampaignType updatedCampaignType = campaignTypeRepository.findOne(campaignType.getId());
        updatedCampaignType
            .name(UPDATED_NAME)
            .marketing(UPDATED_MARKETING);
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(updatedCampaignType);

        restCampaignTypeMockMvc.perform(put("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isOk());

        // Validate the CampaignType in the database
        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeUpdate);
        CampaignType testCampaignType = campaignTypeList.get(campaignTypeList.size() - 1);
        assertThat(testCampaignType.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testCampaignType.isMarketing()).isEqualTo(UPDATED_MARKETING);
    }

    @Test
    public void updateNonExistingCampaignType() throws Exception {
        int databaseSizeBeforeUpdate = campaignTypeRepository.findAll().size();

        // Create the CampaignType
        CampaignTypeDTO campaignTypeDTO = campaignTypeMapper.toDto(campaignType);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCampaignTypeMockMvc.perform(put("/api/campaign-types")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignTypeDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignType in the database
        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCampaignType() throws Exception {
        // Initialize the database
        campaignTypeRepository.save(campaignType);
        int databaseSizeBeforeDelete = campaignTypeRepository.findAll().size();

        // Get the campaignType
        restCampaignTypeMockMvc.perform(delete("/api/campaign-types/{id}", campaignType.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CampaignType> campaignTypeList = campaignTypeRepository.findAll();
        assertThat(campaignTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignType.class);
        CampaignType campaignType1 = new CampaignType();
        campaignType1.setId(UUID.randomUUID());
        CampaignType campaignType2 = new CampaignType();
        campaignType2.setId(campaignType1.getId());
        assertThat(campaignType1).isEqualTo(campaignType2);
        campaignType2.setId(UUID.randomUUID());
        assertThat(campaignType1).isNotEqualTo(campaignType2);
        campaignType1.setId(null);
        assertThat(campaignType1).isNotEqualTo(campaignType2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignTypeDTO.class);
        CampaignTypeDTO campaignTypeDTO1 = new CampaignTypeDTO();
        campaignTypeDTO1.setId(UUID.randomUUID());
        CampaignTypeDTO campaignTypeDTO2 = new CampaignTypeDTO();
        assertThat(campaignTypeDTO1).isNotEqualTo(campaignTypeDTO2);
        campaignTypeDTO2.setId(campaignTypeDTO1.getId());
        assertThat(campaignTypeDTO1).isEqualTo(campaignTypeDTO2);
        campaignTypeDTO2.setId(UUID.randomUUID());
        assertThat(campaignTypeDTO1).isNotEqualTo(campaignTypeDTO2);
        campaignTypeDTO1.setId(null);
        assertThat(campaignTypeDTO1).isNotEqualTo(campaignTypeDTO2);
    }
}
