package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.CampaignStatus;
import com.openmind.primecast.repository.CampaignStatusRepository;
import com.openmind.primecast.service.CampaignStatusService;
import com.openmind.primecast.service.dto.CampaignStatusDTO;
import com.openmind.primecast.service.mapper.CampaignStatusMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the CampaignStatusResource REST controller.
 *
 * @see CampaignStatusResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class CampaignStatusResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private CampaignStatusRepository campaignStatusRepository;

    @Autowired
    private CampaignStatusMapper campaignStatusMapper;

    @Autowired
    private CampaignStatusService campaignStatusService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restCampaignStatusMockMvc;

    private CampaignStatus campaignStatus;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampaignStatusResource campaignStatusResource = new CampaignStatusResource(campaignStatusService);
        this.restCampaignStatusMockMvc = MockMvcBuilders.standaloneSetup(campaignStatusResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static CampaignStatus createEntity() {
        CampaignStatus campaignStatus = new CampaignStatus()
            .name(DEFAULT_NAME);
        return campaignStatus;
    }

    @Before
    public void initTest() {
        campaignStatusRepository.deleteAll();
        campaignStatus = createEntity();
    }

    @Test
    public void createCampaignStatus() throws Exception {
        int databaseSizeBeforeCreate = campaignStatusRepository.findAll().size();

        // Create the CampaignStatus
        CampaignStatusDTO campaignStatusDTO = campaignStatusMapper.toDto(campaignStatus);
        restCampaignStatusMockMvc.perform(post("/api/campaign-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignStatus in the database
        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeCreate + 1);
        CampaignStatus testCampaignStatus = campaignStatusList.get(campaignStatusList.size() - 1);
        assertThat(testCampaignStatus.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    public void createCampaignStatusWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campaignStatusRepository.findAll().size();

        // Create the CampaignStatus with an existing ID
        campaignStatus.setId(UUID.randomUUID());
        CampaignStatusDTO campaignStatusDTO = campaignStatusMapper.toDto(campaignStatus);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampaignStatusMockMvc.perform(post("/api/campaign-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignStatusDTO)))
            .andExpect(status().isBadRequest());

        // Validate the CampaignStatus in the database
        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = campaignStatusRepository.findAll().size();
        // set the field null
        campaignStatus.setName(null);

        // Create the CampaignStatus, which fails.
        CampaignStatusDTO campaignStatusDTO = campaignStatusMapper.toDto(campaignStatus);

        restCampaignStatusMockMvc.perform(post("/api/campaign-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignStatusDTO)))
            .andExpect(status().isBadRequest());

        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllCampaignStatuses() throws Exception {
        // Initialize the database
        campaignStatusRepository.save(campaignStatus);

        // Get all the campaignStatusList
        restCampaignStatusMockMvc.perform(get("/api/campaign-statuses"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campaignStatus.getId().toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    public void getCampaignStatus() throws Exception {
        // Initialize the database
        campaignStatusRepository.save(campaignStatus);

        // Get the campaignStatus
        restCampaignStatusMockMvc.perform(get("/api/campaign-statuses/{id}", campaignStatus.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campaignStatus.getId().toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    public void getNonExistingCampaignStatus() throws Exception {
        // Get the campaignStatus
        restCampaignStatusMockMvc.perform(get("/api/campaign-statuses/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateCampaignStatus() throws Exception {
        // Initialize the database
        campaignStatusRepository.save(campaignStatus);
        int databaseSizeBeforeUpdate = campaignStatusRepository.findAll().size();

        // Update the campaignStatus
        CampaignStatus updatedCampaignStatus = campaignStatusRepository.findOne(campaignStatus.getId());
        updatedCampaignStatus
            .name(UPDATED_NAME);
        CampaignStatusDTO campaignStatusDTO = campaignStatusMapper.toDto(updatedCampaignStatus);

        restCampaignStatusMockMvc.perform(put("/api/campaign-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignStatusDTO)))
            .andExpect(status().isOk());

        // Validate the CampaignStatus in the database
        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeUpdate);
        CampaignStatus testCampaignStatus = campaignStatusList.get(campaignStatusList.size() - 1);
        assertThat(testCampaignStatus.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    public void updateNonExistingCampaignStatus() throws Exception {
        int databaseSizeBeforeUpdate = campaignStatusRepository.findAll().size();

        // Create the CampaignStatus
        CampaignStatusDTO campaignStatusDTO = campaignStatusMapper.toDto(campaignStatus);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCampaignStatusMockMvc.perform(put("/api/campaign-statuses")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campaignStatusDTO)))
            .andExpect(status().isCreated());

        // Validate the CampaignStatus in the database
        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteCampaignStatus() throws Exception {
        // Initialize the database
        campaignStatusRepository.save(campaignStatus);
        int databaseSizeBeforeDelete = campaignStatusRepository.findAll().size();

        // Get the campaignStatus
        restCampaignStatusMockMvc.perform(delete("/api/campaign-statuses/{id}", campaignStatus.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<CampaignStatus> campaignStatusList = campaignStatusRepository.findAll();
        assertThat(campaignStatusList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignStatus.class);
        CampaignStatus campaignStatus1 = new CampaignStatus();
        campaignStatus1.setId(UUID.randomUUID());
        CampaignStatus campaignStatus2 = new CampaignStatus();
        campaignStatus2.setId(campaignStatus1.getId());
        assertThat(campaignStatus1).isEqualTo(campaignStatus2);
        campaignStatus2.setId(UUID.randomUUID());
        assertThat(campaignStatus1).isNotEqualTo(campaignStatus2);
        campaignStatus1.setId(null);
        assertThat(campaignStatus1).isNotEqualTo(campaignStatus2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(CampaignStatusDTO.class);
        CampaignStatusDTO campaignStatusDTO1 = new CampaignStatusDTO();
        campaignStatusDTO1.setId(UUID.randomUUID());
        CampaignStatusDTO campaignStatusDTO2 = new CampaignStatusDTO();
        assertThat(campaignStatusDTO1).isNotEqualTo(campaignStatusDTO2);
        campaignStatusDTO2.setId(campaignStatusDTO1.getId());
        assertThat(campaignStatusDTO1).isEqualTo(campaignStatusDTO2);
        campaignStatusDTO2.setId(UUID.randomUUID());
        assertThat(campaignStatusDTO1).isNotEqualTo(campaignStatusDTO2);
        campaignStatusDTO1.setId(null);
        assertThat(campaignStatusDTO1).isNotEqualTo(campaignStatusDTO2);
    }
}
