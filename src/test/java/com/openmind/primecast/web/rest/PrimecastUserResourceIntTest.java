package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.PrimecastUser;
import com.openmind.primecast.repository.PrimecastUserRepository;
import com.openmind.primecast.service.PrimecastUserService;
import com.openmind.primecast.service.dto.PrimecastUserDTO;
import com.openmind.primecast.service.mapper.PrimecastUserMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PrimecastUserResource REST controller.
 *
 * @see PrimecastUserResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class PrimecastUserResourceIntTest extends AbstractCassandraTest {

    private static final UUID DEFAULT_USER = UUID.randomUUID();
    private static final UUID UPDATED_USER = UUID.randomUUID();

    private static final UUID DEFAULT_ACCOUNT = UUID.randomUUID();
    private static final UUID UPDATED_ACCOUNT = UUID.randomUUID();

    private static final UUID DEFAULT_LANGUAGE = UUID.randomUUID();
    private static final UUID UPDATED_LANGUAGE = UUID.randomUUID();

    @Autowired
    private PrimecastUserRepository primecastUserRepository;

    @Autowired
    private PrimecastUserMapper primecastUserMapper;

    @Autowired
    private PrimecastUserService primecastUserService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPrimecastUserMockMvc;

    private PrimecastUser primecastUser;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PrimecastUserResource primecastUserResource = new PrimecastUserResource(primecastUserService);
        this.restPrimecastUserMockMvc = MockMvcBuilders.standaloneSetup(primecastUserResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrimecastUser createEntity() {
        PrimecastUser primecastUser = new PrimecastUser()
            .user(DEFAULT_USER)
            .account(DEFAULT_ACCOUNT)
            .language(DEFAULT_LANGUAGE);
        return primecastUser;
    }

    @Before
    public void initTest() {
        primecastUserRepository.deleteAll();
        primecastUser = createEntity();
    }

    @Test
    public void createPrimecastUser() throws Exception {
        int databaseSizeBeforeCreate = primecastUserRepository.findAll().size();

        // Create the PrimecastUser
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(primecastUser);
        restPrimecastUserMockMvc.perform(post("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isCreated());

        // Validate the PrimecastUser in the database
        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeCreate + 1);
        PrimecastUser testPrimecastUser = primecastUserList.get(primecastUserList.size() - 1);
        assertThat(testPrimecastUser.getUser()).isEqualTo(DEFAULT_USER);
        assertThat(testPrimecastUser.getAccount()).isEqualTo(DEFAULT_ACCOUNT);
        assertThat(testPrimecastUser.getLanguage()).isEqualTo(DEFAULT_LANGUAGE);
    }

    @Test
    public void createPrimecastUserWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = primecastUserRepository.findAll().size();

        // Create the PrimecastUser with an existing ID
        primecastUser.setId(UUID.randomUUID());
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(primecastUser);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrimecastUserMockMvc.perform(post("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PrimecastUser in the database
        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkUserIsRequired() throws Exception {
        int databaseSizeBeforeTest = primecastUserRepository.findAll().size();
        // set the field null
        primecastUser.setUser(null);

        // Create the PrimecastUser, which fails.
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(primecastUser);

        restPrimecastUserMockMvc.perform(post("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isBadRequest());

        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkAccountIsRequired() throws Exception {
        int databaseSizeBeforeTest = primecastUserRepository.findAll().size();
        // set the field null
        primecastUser.setAccount(null);

        // Create the PrimecastUser, which fails.
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(primecastUser);

        restPrimecastUserMockMvc.perform(post("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isBadRequest());

        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllPrimecastUsers() throws Exception {
        // Initialize the database
        primecastUserRepository.save(primecastUser);

        // Get all the primecastUserList
        restPrimecastUserMockMvc.perform(get("/api/primecast-users"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(primecastUser.getId().toString())))
            .andExpect(jsonPath("$.[*].user").value(hasItem(DEFAULT_USER.toString())))
            .andExpect(jsonPath("$.[*].account").value(hasItem(DEFAULT_ACCOUNT.toString())))
            .andExpect(jsonPath("$.[*].language").value(hasItem(DEFAULT_LANGUAGE.toString())));
    }

    @Test
    public void getPrimecastUser() throws Exception {
        // Initialize the database
        primecastUserRepository.save(primecastUser);

        // Get the primecastUser
        restPrimecastUserMockMvc.perform(get("/api/primecast-users/{id}", primecastUser.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(primecastUser.getId().toString()))
            .andExpect(jsonPath("$.user").value(DEFAULT_USER.toString()))
            .andExpect(jsonPath("$.account").value(DEFAULT_ACCOUNT.toString()))
            .andExpect(jsonPath("$.language").value(DEFAULT_LANGUAGE.toString()));
    }

    @Test
    public void getNonExistingPrimecastUser() throws Exception {
        // Get the primecastUser
        restPrimecastUserMockMvc.perform(get("/api/primecast-users/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePrimecastUser() throws Exception {
        // Initialize the database
        primecastUserRepository.save(primecastUser);
        int databaseSizeBeforeUpdate = primecastUserRepository.findAll().size();

        // Update the primecastUser
        PrimecastUser updatedPrimecastUser = primecastUserRepository.findOne(primecastUser.getId());
        updatedPrimecastUser
            .user(UPDATED_USER)
            .account(UPDATED_ACCOUNT)
            .language(UPDATED_LANGUAGE);
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(updatedPrimecastUser);

        restPrimecastUserMockMvc.perform(put("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isOk());

        // Validate the PrimecastUser in the database
        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeUpdate);
        PrimecastUser testPrimecastUser = primecastUserList.get(primecastUserList.size() - 1);
        assertThat(testPrimecastUser.getUser()).isEqualTo(UPDATED_USER);
        assertThat(testPrimecastUser.getAccount()).isEqualTo(UPDATED_ACCOUNT);
        assertThat(testPrimecastUser.getLanguage()).isEqualTo(UPDATED_LANGUAGE);
    }

    @Test
    public void updateNonExistingPrimecastUser() throws Exception {
        int databaseSizeBeforeUpdate = primecastUserRepository.findAll().size();

        // Create the PrimecastUser
        PrimecastUserDTO primecastUserDTO = primecastUserMapper.toDto(primecastUser);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPrimecastUserMockMvc.perform(put("/api/primecast-users")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastUserDTO)))
            .andExpect(status().isCreated());

        // Validate the PrimecastUser in the database
        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePrimecastUser() throws Exception {
        // Initialize the database
        primecastUserRepository.save(primecastUser);
        int databaseSizeBeforeDelete = primecastUserRepository.findAll().size();

        // Get the primecastUser
        restPrimecastUserMockMvc.perform(delete("/api/primecast-users/{id}", primecastUser.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PrimecastUser> primecastUserList = primecastUserRepository.findAll();
        assertThat(primecastUserList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrimecastUser.class);
        PrimecastUser primecastUser1 = new PrimecastUser();
        primecastUser1.setId(UUID.randomUUID());
        PrimecastUser primecastUser2 = new PrimecastUser();
        primecastUser2.setId(primecastUser1.getId());
        assertThat(primecastUser1).isEqualTo(primecastUser2);
        primecastUser2.setId(UUID.randomUUID());
        assertThat(primecastUser1).isNotEqualTo(primecastUser2);
        primecastUser1.setId(null);
        assertThat(primecastUser1).isNotEqualTo(primecastUser2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrimecastUserDTO.class);
        PrimecastUserDTO primecastUserDTO1 = new PrimecastUserDTO();
        primecastUserDTO1.setId(UUID.randomUUID());
        PrimecastUserDTO primecastUserDTO2 = new PrimecastUserDTO();
        assertThat(primecastUserDTO1).isNotEqualTo(primecastUserDTO2);
        primecastUserDTO2.setId(primecastUserDTO1.getId());
        assertThat(primecastUserDTO1).isEqualTo(primecastUserDTO2);
        primecastUserDTO2.setId(UUID.randomUUID());
        assertThat(primecastUserDTO1).isNotEqualTo(primecastUserDTO2);
        primecastUserDTO1.setId(null);
        assertThat(primecastUserDTO1).isNotEqualTo(primecastUserDTO2);
    }
}
