package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.PrimecastAccount;
import com.openmind.primecast.repository.PrimecastAccountRepository;
import com.openmind.primecast.service.PrimecastAccountService;
import com.openmind.primecast.service.dto.PrimecastAccountDTO;
import com.openmind.primecast.service.mapper.PrimecastAccountMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the PrimecastAccountResource REST controller.
 *
 * @see PrimecastAccountResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class PrimecastAccountResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_1 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_1 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_2 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_2 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_3 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_3 = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS_4 = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS_4 = "BBBBBBBBBB";

    private static final String DEFAULT_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY = "BBBBBBBBBB";

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Boolean DEFAULT_INTERNAL = false;
    private static final Boolean UPDATED_INTERNAL = true;

    private static final UUID DEFAULT_CURRENCY = UUID.randomUUID();
    private static final UUID UPDATED_CURRENCY = UUID.randomUUID();

    private static final UUID DEFAULT_DEFAULT_LANGUAGE = UUID.randomUUID();
    private static final UUID UPDATED_DEFAULT_LANGUAGE = UUID.randomUUID();

    @Autowired
    private PrimecastAccountRepository primecastAccountRepository;

    @Autowired
    private PrimecastAccountMapper primecastAccountMapper;

    @Autowired
    private PrimecastAccountService primecastAccountService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restPrimecastAccountMockMvc;

    private PrimecastAccount primecastAccount;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PrimecastAccountResource primecastAccountResource = new PrimecastAccountResource(primecastAccountService);
        this.restPrimecastAccountMockMvc = MockMvcBuilders.standaloneSetup(primecastAccountResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PrimecastAccount createEntity() {
        PrimecastAccount primecastAccount = new PrimecastAccount()
            .name(DEFAULT_NAME)
            .address1(DEFAULT_ADDRESS_1)
            .address2(DEFAULT_ADDRESS_2)
            .address3(DEFAULT_ADDRESS_3)
            .address4(DEFAULT_ADDRESS_4)
            .country(DEFAULT_COUNTRY)
            .status(DEFAULT_STATUS)
            .internal(DEFAULT_INTERNAL)
            .currency(DEFAULT_CURRENCY)
            .defaultLanguage(DEFAULT_DEFAULT_LANGUAGE);
        return primecastAccount;
    }

    @Before
    public void initTest() {
        primecastAccountRepository.deleteAll();
        primecastAccount = createEntity();
    }

    @Test
    public void createPrimecastAccount() throws Exception {
        int databaseSizeBeforeCreate = primecastAccountRepository.findAll().size();

        // Create the PrimecastAccount
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);
        restPrimecastAccountMockMvc.perform(post("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the PrimecastAccount in the database
        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeCreate + 1);
        PrimecastAccount testPrimecastAccount = primecastAccountList.get(primecastAccountList.size() - 1);
        assertThat(testPrimecastAccount.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPrimecastAccount.getAddress1()).isEqualTo(DEFAULT_ADDRESS_1);
        assertThat(testPrimecastAccount.getAddress2()).isEqualTo(DEFAULT_ADDRESS_2);
        assertThat(testPrimecastAccount.getAddress3()).isEqualTo(DEFAULT_ADDRESS_3);
        assertThat(testPrimecastAccount.getAddress4()).isEqualTo(DEFAULT_ADDRESS_4);
        assertThat(testPrimecastAccount.getCountry()).isEqualTo(DEFAULT_COUNTRY);
        assertThat(testPrimecastAccount.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testPrimecastAccount.isInternal()).isEqualTo(DEFAULT_INTERNAL);
        assertThat(testPrimecastAccount.getCurrency()).isEqualTo(DEFAULT_CURRENCY);
        assertThat(testPrimecastAccount.getDefaultLanguage()).isEqualTo(DEFAULT_DEFAULT_LANGUAGE);
    }

    @Test
    public void createPrimecastAccountWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = primecastAccountRepository.findAll().size();

        // Create the PrimecastAccount with an existing ID
        primecastAccount.setId(UUID.randomUUID());
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPrimecastAccountMockMvc.perform(post("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PrimecastAccount in the database
        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = primecastAccountRepository.findAll().size();
        // set the field null
        primecastAccount.setName(null);

        // Create the PrimecastAccount, which fails.
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);

        restPrimecastAccountMockMvc.perform(post("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isBadRequest());

        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkStatusIsRequired() throws Exception {
        int databaseSizeBeforeTest = primecastAccountRepository.findAll().size();
        // set the field null
        primecastAccount.setStatus(null);

        // Create the PrimecastAccount, which fails.
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);

        restPrimecastAccountMockMvc.perform(post("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isBadRequest());

        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkInternalIsRequired() throws Exception {
        int databaseSizeBeforeTest = primecastAccountRepository.findAll().size();
        // set the field null
        primecastAccount.setInternal(null);

        // Create the PrimecastAccount, which fails.
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);

        restPrimecastAccountMockMvc.perform(post("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isBadRequest());

        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllPrimecastAccounts() throws Exception {
        // Initialize the database
        primecastAccountRepository.save(primecastAccount);

        // Get all the primecastAccountList
        restPrimecastAccountMockMvc.perform(get("/api/primecast-accounts"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(primecastAccount.getId().toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].address1").value(hasItem(DEFAULT_ADDRESS_1.toString())))
            .andExpect(jsonPath("$.[*].address2").value(hasItem(DEFAULT_ADDRESS_2.toString())))
            .andExpect(jsonPath("$.[*].address3").value(hasItem(DEFAULT_ADDRESS_3.toString())))
            .andExpect(jsonPath("$.[*].address4").value(hasItem(DEFAULT_ADDRESS_4.toString())))
            .andExpect(jsonPath("$.[*].country").value(hasItem(DEFAULT_COUNTRY.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].internal").value(hasItem(DEFAULT_INTERNAL.booleanValue())))
            .andExpect(jsonPath("$.[*].currency").value(hasItem(DEFAULT_CURRENCY.toString())))
            .andExpect(jsonPath("$.[*].defaultLanguage").value(hasItem(DEFAULT_DEFAULT_LANGUAGE.toString())));
    }

    @Test
    public void getPrimecastAccount() throws Exception {
        // Initialize the database
        primecastAccountRepository.save(primecastAccount);

        // Get the primecastAccount
        restPrimecastAccountMockMvc.perform(get("/api/primecast-accounts/{id}", primecastAccount.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(primecastAccount.getId().toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.address1").value(DEFAULT_ADDRESS_1.toString()))
            .andExpect(jsonPath("$.address2").value(DEFAULT_ADDRESS_2.toString()))
            .andExpect(jsonPath("$.address3").value(DEFAULT_ADDRESS_3.toString()))
            .andExpect(jsonPath("$.address4").value(DEFAULT_ADDRESS_4.toString()))
            .andExpect(jsonPath("$.country").value(DEFAULT_COUNTRY.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.internal").value(DEFAULT_INTERNAL.booleanValue()))
            .andExpect(jsonPath("$.currency").value(DEFAULT_CURRENCY.toString()))
            .andExpect(jsonPath("$.defaultLanguage").value(DEFAULT_DEFAULT_LANGUAGE.toString()));
    }

    @Test
    public void getNonExistingPrimecastAccount() throws Exception {
        // Get the primecastAccount
        restPrimecastAccountMockMvc.perform(get("/api/primecast-accounts/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updatePrimecastAccount() throws Exception {
        // Initialize the database
        primecastAccountRepository.save(primecastAccount);
        int databaseSizeBeforeUpdate = primecastAccountRepository.findAll().size();

        // Update the primecastAccount
        PrimecastAccount updatedPrimecastAccount = primecastAccountRepository.findOne(primecastAccount.getId());
        updatedPrimecastAccount
            .name(UPDATED_NAME)
            .address1(UPDATED_ADDRESS_1)
            .address2(UPDATED_ADDRESS_2)
            .address3(UPDATED_ADDRESS_3)
            .address4(UPDATED_ADDRESS_4)
            .country(UPDATED_COUNTRY)
            .status(UPDATED_STATUS)
            .internal(UPDATED_INTERNAL)
            .currency(UPDATED_CURRENCY)
            .defaultLanguage(UPDATED_DEFAULT_LANGUAGE);
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(updatedPrimecastAccount);

        restPrimecastAccountMockMvc.perform(put("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isOk());

        // Validate the PrimecastAccount in the database
        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeUpdate);
        PrimecastAccount testPrimecastAccount = primecastAccountList.get(primecastAccountList.size() - 1);
        assertThat(testPrimecastAccount.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPrimecastAccount.getAddress1()).isEqualTo(UPDATED_ADDRESS_1);
        assertThat(testPrimecastAccount.getAddress2()).isEqualTo(UPDATED_ADDRESS_2);
        assertThat(testPrimecastAccount.getAddress3()).isEqualTo(UPDATED_ADDRESS_3);
        assertThat(testPrimecastAccount.getAddress4()).isEqualTo(UPDATED_ADDRESS_4);
        assertThat(testPrimecastAccount.getCountry()).isEqualTo(UPDATED_COUNTRY);
        assertThat(testPrimecastAccount.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testPrimecastAccount.isInternal()).isEqualTo(UPDATED_INTERNAL);
        assertThat(testPrimecastAccount.getCurrency()).isEqualTo(UPDATED_CURRENCY);
        assertThat(testPrimecastAccount.getDefaultLanguage()).isEqualTo(UPDATED_DEFAULT_LANGUAGE);
    }

    @Test
    public void updateNonExistingPrimecastAccount() throws Exception {
        int databaseSizeBeforeUpdate = primecastAccountRepository.findAll().size();

        // Create the PrimecastAccount
        PrimecastAccountDTO primecastAccountDTO = primecastAccountMapper.toDto(primecastAccount);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPrimecastAccountMockMvc.perform(put("/api/primecast-accounts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(primecastAccountDTO)))
            .andExpect(status().isCreated());

        // Validate the PrimecastAccount in the database
        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deletePrimecastAccount() throws Exception {
        // Initialize the database
        primecastAccountRepository.save(primecastAccount);
        int databaseSizeBeforeDelete = primecastAccountRepository.findAll().size();

        // Get the primecastAccount
        restPrimecastAccountMockMvc.perform(delete("/api/primecast-accounts/{id}", primecastAccount.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<PrimecastAccount> primecastAccountList = primecastAccountRepository.findAll();
        assertThat(primecastAccountList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrimecastAccount.class);
        PrimecastAccount primecastAccount1 = new PrimecastAccount();
        primecastAccount1.setId(UUID.randomUUID());
        PrimecastAccount primecastAccount2 = new PrimecastAccount();
        primecastAccount2.setId(primecastAccount1.getId());
        assertThat(primecastAccount1).isEqualTo(primecastAccount2);
        primecastAccount2.setId(UUID.randomUUID());
        assertThat(primecastAccount1).isNotEqualTo(primecastAccount2);
        primecastAccount1.setId(null);
        assertThat(primecastAccount1).isNotEqualTo(primecastAccount2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(PrimecastAccountDTO.class);
        PrimecastAccountDTO primecastAccountDTO1 = new PrimecastAccountDTO();
        primecastAccountDTO1.setId(UUID.randomUUID());
        PrimecastAccountDTO primecastAccountDTO2 = new PrimecastAccountDTO();
        assertThat(primecastAccountDTO1).isNotEqualTo(primecastAccountDTO2);
        primecastAccountDTO2.setId(primecastAccountDTO1.getId());
        assertThat(primecastAccountDTO1).isEqualTo(primecastAccountDTO2);
        primecastAccountDTO2.setId(UUID.randomUUID());
        assertThat(primecastAccountDTO1).isNotEqualTo(primecastAccountDTO2);
        primecastAccountDTO1.setId(null);
        assertThat(primecastAccountDTO1).isNotEqualTo(primecastAccountDTO2);
    }
}
