package com.openmind.primecast.web.rest;

import com.openmind.primecast.AbstractCassandraTest;
import com.openmind.primecast.PrimecastApp;

import com.openmind.primecast.domain.ContactList;
import com.openmind.primecast.repository.ContactListRepository;
import com.openmind.primecast.service.ContactListService;
import com.openmind.primecast.service.dto.ContactListDTO;
import com.openmind.primecast.service.mapper.ContactListMapper;
import com.openmind.primecast.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;
import java.util.UUID;

import static com.openmind.primecast.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ContactListResource REST controller.
 *
 * @see ContactListResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = PrimecastApp.class)
public class ContactListResourceIntTest extends AbstractCassandraTest {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private ContactListRepository contactListRepository;

    @Autowired
    private ContactListMapper contactListMapper;

    @Autowired
    private ContactListService contactListService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    private MockMvc restContactListMockMvc;

    private ContactList contactList;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContactListResource contactListResource = new ContactListResource(contactListService);
        this.restContactListMockMvc = MockMvcBuilders.standaloneSetup(contactListResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ContactList createEntity() {
        ContactList contactList = new ContactList()
            .type(DEFAULT_TYPE)
            .path(DEFAULT_PATH);
        return contactList;
    }

    @Before
    public void initTest() {
        contactListRepository.deleteAll();
        contactList = createEntity();
    }

    @Test
    public void createContactList() throws Exception {
        int databaseSizeBeforeCreate = contactListRepository.findAll().size();

        // Create the ContactList
        ContactListDTO contactListDTO = contactListMapper.toDto(contactList);
        restContactListMockMvc.perform(post("/api/contact-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactListDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactList in the database
        List<ContactList> contactListList = contactListRepository.findAll();
        assertThat(contactListList).hasSize(databaseSizeBeforeCreate + 1);
        ContactList testContactList = contactListList.get(contactListList.size() - 1);
        assertThat(testContactList.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testContactList.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    public void createContactListWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactListRepository.findAll().size();

        // Create the ContactList with an existing ID
        contactList.setId(UUID.randomUUID());
        ContactListDTO contactListDTO = contactListMapper.toDto(contactList);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactListMockMvc.perform(post("/api/contact-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactListDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ContactList in the database
        List<ContactList> contactListList = contactListRepository.findAll();
        assertThat(contactListList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    public void getAllContactLists() throws Exception {
        // Initialize the database
        contactListRepository.save(contactList);

        // Get all the contactListList
        restContactListMockMvc.perform(get("/api/contact-lists"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contactList.getId().toString())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE.toString())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH.toString())));
    }

    @Test
    public void getContactList() throws Exception {
        // Initialize the database
        contactListRepository.save(contactList);

        // Get the contactList
        restContactListMockMvc.perform(get("/api/contact-lists/{id}", contactList.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contactList.getId().toString()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE.toString()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH.toString()));
    }

    @Test
    public void getNonExistingContactList() throws Exception {
        // Get the contactList
        restContactListMockMvc.perform(get("/api/contact-lists/{id}", UUID.randomUUID().toString()))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateContactList() throws Exception {
        // Initialize the database
        contactListRepository.save(contactList);
        int databaseSizeBeforeUpdate = contactListRepository.findAll().size();

        // Update the contactList
        ContactList updatedContactList = contactListRepository.findOne(contactList.getId());
        updatedContactList
            .type(UPDATED_TYPE)
            .path(UPDATED_PATH);
        ContactListDTO contactListDTO = contactListMapper.toDto(updatedContactList);

        restContactListMockMvc.perform(put("/api/contact-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactListDTO)))
            .andExpect(status().isOk());

        // Validate the ContactList in the database
        List<ContactList> contactListList = contactListRepository.findAll();
        assertThat(contactListList).hasSize(databaseSizeBeforeUpdate);
        ContactList testContactList = contactListList.get(contactListList.size() - 1);
        assertThat(testContactList.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testContactList.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    public void updateNonExistingContactList() throws Exception {
        int databaseSizeBeforeUpdate = contactListRepository.findAll().size();

        // Create the ContactList
        ContactListDTO contactListDTO = contactListMapper.toDto(contactList);

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restContactListMockMvc.perform(put("/api/contact-lists")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contactListDTO)))
            .andExpect(status().isCreated());

        // Validate the ContactList in the database
        List<ContactList> contactListList = contactListRepository.findAll();
        assertThat(contactListList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    public void deleteContactList() throws Exception {
        // Initialize the database
        contactListRepository.save(contactList);
        int databaseSizeBeforeDelete = contactListRepository.findAll().size();

        // Get the contactList
        restContactListMockMvc.perform(delete("/api/contact-lists/{id}", contactList.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<ContactList> contactListList = contactListRepository.findAll();
        assertThat(contactListList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactList.class);
        ContactList contactList1 = new ContactList();
        contactList1.setId(UUID.randomUUID());
        ContactList contactList2 = new ContactList();
        contactList2.setId(contactList1.getId());
        assertThat(contactList1).isEqualTo(contactList2);
        contactList2.setId(UUID.randomUUID());
        assertThat(contactList1).isNotEqualTo(contactList2);
        contactList1.setId(null);
        assertThat(contactList1).isNotEqualTo(contactList2);
    }

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ContactListDTO.class);
        ContactListDTO contactListDTO1 = new ContactListDTO();
        contactListDTO1.setId(UUID.randomUUID());
        ContactListDTO contactListDTO2 = new ContactListDTO();
        assertThat(contactListDTO1).isNotEqualTo(contactListDTO2);
        contactListDTO2.setId(contactListDTO1.getId());
        assertThat(contactListDTO1).isEqualTo(contactListDTO2);
        contactListDTO2.setId(UUID.randomUUID());
        assertThat(contactListDTO1).isNotEqualTo(contactListDTO2);
        contactListDTO1.setId(null);
        assertThat(contactListDTO1).isNotEqualTo(contactListDTO2);
    }
}
